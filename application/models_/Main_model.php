<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Main_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->library('session');

	}

	function cek_login(){

		$check = array(
						'username' => $this->input->post('username'), 
						'password' => $this->input->post('password')
					);
		/*check if username is exist*/
		$sql_un 	= "SELECT * FROM ms_login WHERE username = ? AND password = ? AND del = 0";
		$sql_un 	= $this->db->query($sql_un, $check);
		$sql_un 	= $sql_un->row_array();

		$ct_sql 	= '';

		if ($sql_un) {
			/*set the session*/
			$set_session = array(
				'id' 			=> 	$sql_un['id'],
				'username'		=>	$sql_un['username'],
				'password'		=>  $sql_un['password'],
			);
			$this->session->set_userdata('admin',$set_session);
			return "ok";
		}
	}
	
}