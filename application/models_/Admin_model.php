<?php defined('BASEPATH') OR exit('No direct script access allowed');


class admin_model extends CI_Model{

	function __construct(){

		parent::__construct();
		// $this->load->library('database')
		$this->pedoman = array(
			'name',
			'entry_stamp'
			);
		$this->pedoman_struktur = array(
			'name',
			'digit',
			'entry_stamp'
			);

	}

	/*==============================================
						TABEL
	==============================================*/
	function get_table($search='', $sort='', $page='', $per_page='',$is_page=FALSE,$filter=array()){
		$this->db
				->select('ms_table.*')
				// ->where('del', 0)
				->order_by('id', 'ASC');

		if($this->input->get('sort')&&$this->input->get('by')){
			$this->db->order_by($this->input->get('by'), $this->input->get('sort')); 
		}
		if($is_page){
			$cur_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
			$this->db->limit($per_page, $per_page*($cur_page - 1));
		}

		$a = $this->db->group_by('ms_table.id');
		$query = $a->get('ms_table');


		return $query->result_array();
	}
	function get_table_detail($id_table){
		$this->db
				->select('ms_table.name table, ms_table_detail.*')
				->join('ms_table','ms_table.id = ms_table_detail.id_table')
				->where('ms_table.id', $id_table)
				->where('ms_table_detail.del', 0)
				->order_by('code', 'ASC');

		$a 		= $this->db->group_by('ms_table_detail.id');
		$query['data'] = $a->get('ms_table_detail')->result_array();

		$query['table'] = $this->db->select('name')->where('id', $id_table)->get('ms_table')->row_array();
		$query['table'] = $query['table']['name'];
		// print_r($query);

		return $query;
	}

	function get_per_row($id_row){
		$id_row = $this->db->where('id',$id_row)
				->where('ms_table_detail.del', 0)->get('ms_table_detail', $id_row)->row_array();
		$this->db
				->select('ms_table.name table, ms_table_detail.*, ms_table_header.name header')
				->join('ms_table','ms_table.id = ms_table_detail.id_table')
				->join('ms_table_header','ms_table_header.id = ms_table_detail.id_header')
				// ->where('ms_table.id', $id_table)
				->where('ms_table_detail.row', $id_row['row'])
				->where('ms_table_detail.id_table', $id_row['id_table'])
				->order_by('code', 'ASC');

		$a 		= $this->db->group_by('ms_table_detail.id');
		$query['data'] = $a->get('ms_table_detail')->result_array();

		$query['table'] = $this->db->select('name')->where('id', $id_row['id_table'])->get('ms_table')->row_array();
		$query['table'] = $query['table']['name'];
		// print_r($query);

		return $query;
	}
	function get_table_header($id_table){
		$this->db
				->select('ms_table.name table, ms_table_header.*')
				->join('ms_table','ms_table.id = ms_table_header.id_table')
				->where('ms_table.id', $id_table)
				->order_by('code', 'ASC');

		$a 		= $this->db->group_by('ms_table_header.id');
		$query['header'] = $a->get('ms_table_header')->result_array();

		$query['table'] = $this->db->select('name')->where('id', $id_table)->get('ms_table')->row_array();
		$query['table'] = $query['table']['name'];

		$table = "<table class='table'>";
		$table 	.= "<thead>";
		$table 	.= "<tr>";
		foreach ($query['header'] as $key => $value) {$table 	.= "<td>".$value['name']."</td>";}
		$table  .= "<td>ACTION</td>";
		$table  .= "</tr>";
		$table 	.= "</thead>";

		$column = $this->db->where('id_table', $id_table)->group_by('id')->get('ms_table_header')->result_array();
		$detail = $this->db->where('id_table', $id_table)->get('ms_table_detail')->result_array();
		$row    = $this->db->where('id_table', $id_table)->group_by('row')->get('ms_table_detail')->result_array();

		foreach ($query['header'] as $key => $header) {
			# code...
			$sql = $this->db->where('id_header', $header['id'])->get('ms_table_detail')->result_array();
			$table .= "<tr>";
		}

		#ROW
		$table .= "<tbody class='table-body'>";
		foreach ($row as $key_ => $value_) {
			#column
			$table .= "<tr>";
			foreach ($column as $key => $header) {
				# code...
				$sql = $this->db->where('id_header', $header['id'])->where('row', $value_['row'])->get('ms_table_detail')->row_array();
				$table .= "<td>".$sql['name']."</td>";
			}
			$table .= "<td><a href='".base_url('admin/edit_row/')."/".$sql['id']."'><i class='fa fa-edit'></i></a><a onclick='return confirm('Hapus Item?')' href='".base_url('admin/delete_row/')."/".$sql['id']."'><i class='fa fa-close'></i></a></td>";
			$table .= "</tr>";
		}
		$table .= "</tbody>";
		$table .= "</table>";
		// print_r($table);die;


		$result['table'] 	= $table;
		$result['row'] 		= count($row);
		$result['column'] 	= $column;		

		return $result;
	}


	/*==============================================
					pedoman & EVENT
	==============================================*/
	function get_pedoman($search='', $sort='', $page='', $per_page='',$is_page=FALSE,$filter=array()){
		$this->db
				->select('ms_pedoman.*')
				->where('del', 0)
				->order_by('id', 'ASC');

		if($this->input->get('sort')&&$this->input->get('by')){
			$this->db->order_by($this->input->get('by'), $this->input->get('sort')); 
		}
		if($is_page){
			$cur_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
			$this->db->limit($per_page, $per_page*($cur_page - 1));
		}

		$a = $this->db->group_by('ms_pedoman.id');
		$query = $a->get('ms_pedoman');


		return $query->result_array();
	}

	function get_per_pedoman($id){
		return $this->db
					->select('ms_pedoman_struktur.*, ms_pedoman.name pedoman, ms_table.name table')
					// ->where('ms_pedoman.del', 0)
					->where('id_pedoman', $id)
					->join('ms_pedoman', 'ms_pedoman.id = ms_pedoman_struktur.id_pedoman')
					->join('ms_table', 'ms_table.id = ms_pedoman_struktur.id_table')
					->get('ms_pedoman_struktur')
					->row_array();
	}

	function save_pedoman(){
		// print_r($this->session->userdata());

		$pedoman 			= $this->session->userdata('pedoman');
		$pedoman_struktur 	= $this->session->userdata('pedoman_struktur');

		// print_r($pedoman_struktur);
		#SAVE PEDOMAN
		$pedoman['del'] = 0;
		$save_pedoman   = $this->db->insert('ms_pedoman', $pedoman);
		$id_pedoman 	= $this->db->insert_id();
		
		$data_ = "";
		#SAVE STRUKTUR PEDOMAN
		foreach ($pedoman_struktur as $column => $data) {
			# code...
			foreach ($data as $key => $value) {
				$data_[$key][$column] .= $value;
			}
		}

		foreach ($data_ as $key => $value) {
			# code...
			$data_ps['id_pedoman'] 	= $id_pedoman;
			$data_ps['name'] 		= $value['name'];
			$data_ps['digit'] 		= $value['digit'];  
			$this->db->insert('ms_pedoman_struktur', $data_ps);
		}

		#UNSET THE session
		$this->session->unset_userdata('pedoman');
		$this->session->unset_userdata('pedoman_struktur');
		return $id_pedoman;
	}

	function edit_pedoman($data,$id){
		$this->db->where('id',$id);
		

		$result = $this->db->update('ms_pedoman',$data);
		if($result)return $id;
	}

	function delete_pedoman($id){
		$this->db->where('id',$id);
		
		return $this->db->update('ms_pedoman',array('del'=>1));
	}

	function edit_row($data,$id){
		$this->db->where('id',$id);
		

		$result = $this->db->update('ms_table_detail',$data);
		if($result)return $id;
	}
	function delete_row($id){
		$this->db->where('id',$id);
		$this->db->update('ms_table_detail',array('del'=>1));
		
		$id_table = $this->db->where('id', $id)->get('ms_table_detail')->row_array();
		return $id_table['id_table'];
	}

	function get_pedoman_struktur($id_pedoman){

		$get = $this->db->select('id,name')->where('id_pedoman', $id_pedoman)->get('ms_pedoman_struktur');
		$raw = $get->result_array();
		$res = array();

		$res[''] = 'Pilih salah satu';
		foreach($raw as $key => $val){
			$res[$val['id']] = $val['name'];
		}
		
		return $res;
	
	}
	function get_table_id(){

		$get = $this->db->select('id,name')->get('ms_table');
		$raw = $get->result_array();
		$res = array();

		$res[''] = 'Pilih salah satu';
		foreach($raw as $key => $val){
			$res[$val['id']] = $val['name'];
		}
		
		return $res;
	
	}

	function save_komag($data){ 
		$this->db->insert('ms_komag_detail', $data_komag);
		$id_komag = $this->db->insert_id();

		return $this->db->insert_id();

	}

	function get_pedoman_($id_pedoman){
		$data = $this->db->select('*')
					->where('ms_pedoman.id', $id_pedoman)
					->get('ms_pedoman');
					// print_r($data->result_array());die;
		return $data->row_array();
	}

	function get_per_pedoman_struktur($id_pedoman){

		$get = $this->db->select('id,name')->where('id_pedoman', $id_pedoman)->get('ms_pedoman_struktur');
		$raw = $get->result_array();
		
		return $raw;
	
	}

	function get_komag($id_pedoman){
		$data = $this->db->select('ms_pedoman_struktur.id id_pedoman_struktur, ms_pedoman_struktur.name pedoman_struktur, ms_komag_detail.id id_komag, ms_table.name table, ms_table.id id_table')
					->join('ms_pedoman_struktur', 'ms_pedoman_struktur.id_pedoman = ms_pedoman.id')
					->join('ms_komag_detail', 'ms_komag_detail.id_pedoman_struktur = ms_pedoman_struktur.id', 'LEFT')
					->join('ms_table', 'ms_table.id = ms_komag_detail.id_table', 'LEFT')
					->where('ms_pedoman.id', $id_pedoman)
					->group_by('ms_pedoman_struktur.id')
					->get('ms_pedoman');

		return $data->result_array();
	}
	function get_all_komag(){
		$data = $this->db->get('ms_komag');

		return $data->result_array();
	}

	function get_material(){
		$data = $this->db->select('*')->get('tb_material');

		return $data->result_array();
	}


	function get_select_id($table=""){

		$get = $this->db->select('id,name')->get($table);
		$raw = $get->result_array();
		$res = array();

		$res[''] = 'Pilih salah satu';
		foreach($raw as $key => $val){
			$res[$val['id']] = $val['name'];
		}
		
		return $res;
	}

	function get_material_id(){

		$get = $this->db->select('id,code,name')->get("tb_material");
		$raw = $get->result_array();
		$res = array();

		$res[''] = 'Pilih salah satu';
		foreach($raw as $key => $val){
			$res[$val['id']] = $val['code']." - ".$val['name'];
		}
		
		return $res;
	}



	function get_master_komag($id_material){
		$data = $this->db->select('*');
					
		if ($id_material > 0 ){
			$data = $data->where('id_material', $id_material);
		}

		$data = $data->get('ms_komag')->result_array();

		$html_ 	= '<html><head><meta http-equiv="Content-Type" content="text/html;charset=windows-1252"><title>MASTERKOMAGFINAL</title></head><body><font face="Calibri" color="#000000"></font><table border="1" bgcolor="#ffffff" cellspacing="0"><caption><b>MASTERKOMAGFINAL</b></caption>';
		$html_ .= "<tr>";
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">NO</td>';
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">KOMAG</td>';
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">UOM</td>';
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">SHORT DESCRIPTION</td>';
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">LONG DESCRIPTION</td>';
		$html_ .= '<th bgcolor="#c0c0c0" bordercolor="#000000"><font style="FONT-SIZE:11pt" face="Calibri" color="#000000">KELOMPOK</td>';
		$html_ .= "</tr>";

		if (count($data)) {
			# code...
			foreach ($data as $key => $value) {
				$no = $key +1;
				$html_ .= "<tr>";
				$html_ .= "<td align='left'>".$no."</td>";
				$html_ .= "<td align='left'>".$value['komag']."</td>";
				$html_ .= "<td align='left'>".$value['uom']."</td>";
				$html_ .= "<td align='left'>".$value['short_desc']."</td>";
				$html_ .= "<td align='left'>".$value['long_desc']."</td>";
				$html_ .= "<td align='left'>".$value['id_material']."</td>";
				$html_ .= "</tr>";
			}
		}else{
			$html_ .= "<tr><td align='left' colspan='6'>Data Kosong</td></tr>";
		}

		print_r($html_);die;
		return $data;
	}


}