<?php defined('BASEPATH') OR exit('No direct script access allowed');

class news extends CI_Controller {
	public function __construct(){

		parent::__construct();

		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel.php");
		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel/IOFactory.php");

		$this->load->library('form', 'database','url');
		$this->load->library('session');
		$this->load->model('Main_model', 'mm');

		$this->data = array(
            'news' 				=> $this->mm->get_all_news(),
            'year' 				=> $this->mm->get_year('ms_coach'),
            'year_atlet'		=> $this->mm->get_year_atlet('ms_athlete'),
            'sport'				=> $this->mm->get_sport()
        );
	}

	public function index(){
		$id = $this->input->get('id');
		$layout					= $this->data;
		$layout['sport'] 		= $this->mm->get_sport();
		if ($id > 0) {
			$data 					= $this->mm->get_news($id);
			$data['recommend']		= $this->mm->get_recommend_news($id);
			// print_r($data);
			$layout['header'] 		= $data['title'];
			$layout['contentArea'] 	= $this->load->view('news/detail',$data,TRUE);
			// $layout['script'] 		= $this->load->view('template/dashboard_js',NULL,TRUE);
			$this->load->view('template/template',$layout);
		}else{
			$data['news'] 			= $this->mm->get_all_news();
			$layout['header'] 		= 'News';
			$layout['contentArea'] 	= $this->load->view('news/view',$data,TRUE);
			// $layout['script'] 		= $this->load->view('template/dashboard_js',NULL,TRUE);
			$this->load->view('template/template',$layout);
		}
	}

}