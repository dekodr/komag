<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Ubah Profil</h1>
        <h3>Welcome, <?php print_r($this->session->userdata('admin')['name']);?><?php print_r($this->session->userdata('user')['name']);?><?php print_r($this->session->userdata('perusahaan')['name']);?>.</h3>


        <div class="lineArea clearfix">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Ubah Profil</h3>
                <form action="" method="POST">
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="username">Username*</label>
                        <input class="textInput" type="text" value="<?php echo $username; ?>" placeholder="username" name="username" required>
                    </div>
                    <div class="inputGroup">
                        <label></label>
                        <div class="textInput" style="background:none; border:none;">
                            <?php echo form_error('username');?>
                        </div>
                    </div>
                    <!--/.inputGroup-->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="password">Password*</label>
                        <input class="textInput" type="password" value="<?php echo $password; ?>" placeholder="password" name="password" required>
                    </div>
                    <div class="inputGroup">
                        <label></label>
                        <div class="textInput" style="background:none; border:none;">
                            <?php echo form_error('password');?>
                        </div>
                    </div>
                    <!--/.inputGroup-->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <input type="hidden" value="<?php echo $id_user; ?>" name="id_user">
                        <button name="simpan" type="submit" class="buttonInput blueBG">
                            Simpan
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->