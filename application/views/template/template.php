<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="theme-color" content="#2980b9" />
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#2980b9">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#2980b9">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#2980b9">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/css/base.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/css/reset.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/css/animate.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/css/font-awesome.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/css/font-awesome.min.css');?>">
    
        <link href="<?php echo base_url('assets/common/slider/jquery.bxslider.css')?>" rel="stylesheet"><!--slider-->

        <link rel="stylesheet" href="<?php echo base_url('assets/common/nav/css/reset.css');?>"> <!-- CSS reset -->
        <link rel="stylesheet" href="<?php echo base_url('assets/common/nav/css/style.css');?>"> <!-- Resource style -->
        <link rel="stylesheet" href="assets/common/js/news-slider/css/vertical.news.slider.css?v=1.0">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/js/cssmenu/styles.css')?>"/>

        
        <script type="text/javascript" src="<?php echo base_url('assets/common/js/jquery-latest.min.js')?>"></script>
        <script src="<?php echo base_url('assets/common/nav/js/modernizr.js');?>"></script> <!-- Modernizr -->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/common/js/jquery.vticker.js');?>"></script>

        <script src="<?php echo base_url('assets/common/nav/js/jquery.mobile.custom.min.js');?>"></script>
        <script src="<?php echo base_url('assets/common/nav/js/main.js');?>"></script> <!-- Resource jQuery -->

        <script type="text/javascript" src="<?php echo base_url('assets/common/js/numeral.min.js')?>"></script>
        <script src="<?php echo base_url('assets/common/slider/jquery.bxslider.min.js')?>"></script><!--slider-->

        <script type="text/javascript" src="<?php echo base_url('assets/common/js/cssmenu/script.js')?>"></script>
        <script src="<?php echo base_url('assets/common/js/stripe-navigation/js/main.js')?>"></script>
        <script src="<?php echo base_url('assets/common/js/stripe-navigation/js/modernizr-custom.js')?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/common/js/stripe-navigation/css/style.css')?>"/>

        <script type="text/javascript">
            $(document).ready(function(){
                
                $("#search-button").click(function(){
                    $(".search-bar").show(400);
                    $(".search-bar-bg").show();
                });
                $("#form-button").click(function(){
                    $(".download-bar").show(400);
                    $(".search-bar-bg").show();
                });

                $(".closeX").click(function(){
                    $(".search-bar").hide(400);
                    $(".download-bar").hide(400);
                    $(".search-bar-bg").hide();
                });
            });
        </script>

        <!--home-->
        <script type="text/javascript">
            $(document).ready(function(){
                $('.bxslider').bxSlider({
                    controls : false,
                    auto : true
                });
            });
        </script>
        <script>
            $(function() {
                $('#news-ticker').vTicker({
                    speed: 700,
                    pause: 4000,
                    showItems: 1,
                    mousePause: true,
                    height: 0,
                    animate: true,
                    margin: 0,
                    padding: 0,
                    startPaused: false
                });
            });
        </script>
        <!--/script-->

        <title><?php echo $header;?></title>
    </head>
    
    <body>

        <!--#wrap-->
        <div id="wrap">
            <!--#wrapInner-->
            <div id="wrapInner">
               
                <!--#headerArea-->
                <div id="header">
                    <img src="<?php echo base_url('assets/common/images/header.jpg')?>" style="width: 100%">
                </div>

                <div id="main-menu">
                    <header class="cd-morph-dropdown">
                        <a href="#0" class="nav-trigger">Open Nav<span aria-hidden="true"></span></a>
                        
                        <nav class="main-nav">
                            <ul>
                                <li><a href="<?php echo base_url();?>">Beranda</a></li>
                                <li class="has-dropdown button" data-content="cabor">
                                    <a href="#0">Cabang Olahraga</a>
                                </li>
                                <li class="has-dropdown button" data-content="pelatih">
                                    <a href="<?php echo base_url('main/search?profesi=pelatih');?>">Pelatih</a>
                                </li>
                                <li  class="has-dropdown button" data-content="atlet">
                                    <a href="<?php echo base_url('main/search?profesi=atlet');?>">Atlet</a>
                                </li>
                                <li class="has-dropdown button" data-content="perangkat">
                                    <a>Perangkat</a>
                                </li>
                                <li class="has-dropdown button" data-content="fasilitas">
                                    <a>Fasilitas</a>
                                </li>
                                <li><a href='<?php echo base_url('main/galery');?>'><span>Galeri</span></a></li>
                                <li><a href="<?php echo base_url('news');?>"><span>Berita</span></a></li>
                                <li id="form-button"><a href='#'><span>Form Pendaftaran</span></a></li>
                                <li><a href="<?php echo base_url('admin');?>"><span>LOGIN</span></a></li>
                            </ul>
                        </nav>
                        
                        <div class="morph-dropdown-wrapper">
                            <div class="dropdown-list">
                                <ul>
                                    <li id="cabor" class="dropdown button">                                       
                                        <div class="content">   
                                            <ul class="links-list">
                                                <?php foreach($sport as $key => $data){ 
                                                    if ($data['id_sport'] == "TENNIS" || $data['id_sport'] == "BULU TANGKIS") {?>
                                                        <li><a href="#" style="color: #333; text-decoration: underline;"><span><?php echo ucwords(strtolower($data['id_sport'])); ?></span></a></li>
                                                    <?php }else{?>
                                                        <li><a href="<?php echo base_url('main/cabor?id='.$data['id_sport'])?>"><span><?php echo ucwords(strtolower($data['id_sport'])); ?></span></a></li>
                                                    <?php }?>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="pelatih" class="dropdown button">
                                        <div class="content">   
                                            <ul class="links-list">
                                                <?php foreach($year as $key => $data){ ?>
                                                <li><a href="<?php echo base_url('main/search?profesi=pelatih&year='.$data['year'])?>"><span><?php echo $data['year']; ?></span></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="atlet" class="dropdown button">
                                        <div class="content">   
                                            <ul class="links-list">
                                                <?php foreach($year_atlet as $key => $data){ ?>
                                                <li><a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span><?php echo $data['year']; ?></span></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="perangkat" class="dropdown button">
                                        <div class="content">   
                                            <ul class="links-list">
                                                <li><a href="<?php echo base_url('main/detail_pengelola/1')?>"><span>Direktur Teknik</span></a></li>
                                                <li><a href="<?php echo base_url('main/search?profesi=pengelola&type=pengelola');?>"><span>Pengelola</span></a></li>                                          
                                                <li><a href="#"><span>Psikolog Olahraga</span></a> </li>
                                                <li><a href="<?php echo base_url('main/search?profesi=guru');?>"><span>Guru Bimbel</span></a> </li>
                                                <li>
                                                    <a href="#<?php echo base_url('main/search?profesi=pengelola&type=');?>"><span>Sport Medicine</span></a> 
                                                    <ul>
                                                        <li>
                                                            <a href="<?php echo base_url('main/search?profesi=pengelola&type=dokter');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span>Dokter</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('main/search?profesi=pengelola&type=masseur');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span>Masseur</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('main/search?profesi=pengelola&type=perawat');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span>Perawat</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('main/search?profesi=pengelola&type=apoteker');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span>Apoteker</span></a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo base_url('main/search?profesi=pengelola&type=fisioterapi');?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<span>Fisioterapi</span></a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="fasilitas" class="dropdown button">
                                        <div class="content">   
                                            <ul class="links-list">
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Gedung 3 Lantai Wisma Atlet.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Perpustakaan.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Klinik Kesehatan.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Klinik Fisiotherapy.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Ruang Rapat.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Ruang Fitness.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Ruang Makan.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Ruang Kantor.</span></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url('main/search?profesi=atlet&year='.$data['year'])?>"><span>Gedung Khusus Cabang Olah Raga Beladiri.</span></a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </li>
                                </ul>

                                <div class="bg-layer" aria-hidden="true"></div>
                            </div> <!-- dropdown-list -->
                        </div> <!-- morph-dropdown-wrapper -->
                    </header>
                </div>

                <div id="search-button" style="z-index : 1000">
                    <div id="icon-big">
                        <i class="fa fa-search"></i>
                        <input id="button" id="button" type="submit" value=" " />
                    </div>
                    <div id="border"></div>
                </div>

                <!--#mainArea-->
                <div id="mainArea">
                    <?php echo $contentArea;?>
                </div>
                <!--/#mainArea-->
                
                <!--#footerArea-->
                <!--/#footerArea-->
                
            </div>
            <!--/#wrapInner-->
        </div>


        <!--/#wrap-->
        <div class="search-bar" style="z-index : 1000000; display : none">
            <a class="closeX" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
            
            <form action="<?php echo base_url('index.php/main/search/')?>" method="GET">
                <div class="row-input clearfix">
                    <label>Profesi</label>
                    <div class="input">
                        <input type="radio" name="profesi" value="atlet" required>Atlet
                        <input type="radio" name="profesi" value="pelatih" required>Pelatih
                        <input type="radio" name="profesi" value="pengelola" required>Perangkat
                        <input type="radio" name="profesi" value="guru" required>Guru Bimbel
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Cabang Olahraga</label>
                    <div class="input">
                        <select name="id_sport">
                            <option value="">ALL</option>
                            <?php foreach($sport as $key => $data){?>
                            <option value="<?php echo $data['id_sport'];?>"><?php echo $data['id_sport'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Nama</label>
                    <div class="input">
                        <input type="text" name="name">
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Tahun Lahir</label>
                    <div class="input">
                        <select name="birth_info">
                            <option value="">All</option>
                            <?php for ($i=2018; $i > 1950; $i--) {?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Tahun Masuk</label>
                    <div class="input">
                        <select name="year">
                            <option value="">All</option>

                            <?php foreach($year as $key => $data){ ?>
                            <option value="<?php echo $data['year']; ?>"><?php echo $data['year']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Periode</label>
                    <div class="input">
                        <input type="radio" value="1" name="periode">1
                        <input type="radio" value="2" name="periode">2
                    </div>
                </div>
                <div class="row-input clearfix">
                    <label>Jenis Kelamin</label>
                    <div class="input">
                        <input type="radio" name="sex" value="l">L
                        <input type="radio" name="sex" value="p">P
                    </div>
                </div>
                <button type="submit" class="search-button">Cari</button>
            </form>
        </div>
        <div class="download-bar" style="z-index : 1000000; display : none">
            <a class="closeX" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
            <p>Calon siswa baru wajib melampirkan:</p>
            <ul>
                <li>a. Formulir pendaftaran</li>
                <li>b. FC Akta Lahir</li>
                <li>c. FC STTB/Ijazah (1 lembar)</li>
                <li>d. FC Prestasi</li>
                <li>e. Surat rekomendasi sekolah</li>
                <li>f. Surat persetujuan orang tua</li>
            </ul>
            <a class="link" href='<?php echo base_url('lampiran/FRM_REG.pdf');?>' target="_blank"><span>DOWNLOAD FORMULIR</span></a>
        </div>
        <div class="search-bar-bg" style="z-index : 1000; display : none"></div>
        
        <script src="<?php echo base_url('assets/common/js/news-slider/js/vertical.news.slider.min.js')?>"></script>
    </body>
</html>
