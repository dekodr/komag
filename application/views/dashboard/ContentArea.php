<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Dashboard <?php print_r($this->session->userdata('admin')['role_name']);?></h1>
        <h3>Welcome to ATK Apps, <?php print_r($this->session->userdata('admin')['name']);?><?php print_r($this->session->userdata('user')['name']);?>.</h3>


        <?php if (($this->session->userdata('user'))=="") {?>
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.boxInfo-->
            <div class="boxInfo">
                <div class="boxInner clearfix">
                    <div class="symbol symbol-sales">
                        <i class="fa fa-industry"></i>
                    </div>

                    <div class="boxTxt">
                        <p>17652</p>
                        <h3>Perusahaan Terdaftar</h3>
                    </div>
                </div>
            </div>
            <!--/.boxInfo-->
            <!--.boxInfo-->
            <div class="boxInfo">
                <div class="boxInner clearfix">
                    <div class="symbol symbol-sales">
                        <i class="fa fa-shopping-cart"></i>
                    </div>

                    <div class="boxTxt">
                        <p>17652</p>
                        <h3>Penjualan Hari Ini</h3>
                    </div>
                </div>
            </div>
            <!--/.boxInfo-->
            <!--.boxInfo-->
            <div class="boxInfo">
                <div class="boxInner clearfix">
                    <div class="symbol symbol-sales">
                        <i class="fa fa-shopping-cart"></i>
                    </div>

                    <div class="boxTxt">
                        <p>17652</p>
                        <h3>Penjualan Hari Ini</h3>
                    </div>
                </div>
            </div>
            <!--/.boxInfo-->
            <!--.boxInfo-->
            <div class="boxInfo">
                <div class="boxInner clearfix">
                    <div class="symbol symbol-sales">
                        <i class="fa fa-shopping-cart"></i>
                    </div>

                    <div class="boxTxt">
                        <p>17652</p>
                        <h3>Penjualan Hari Ini</h3>
                    </div>
                </div>
            </div>
            <!--/.boxInfo-->
        </div>
        <!--/.lineArea-->
        <?php }?>


        <!--.lineArea-->
        <div class="lineArea clearfix" id="kelola_pesanan">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Pesanan</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">#</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Nama Pemesan</a><i class="fa fa-sort-desc"></i></td>
                            <?php if (($this->session->userdata('user'))!="") {?>
                            <td><a href="#">Progress</a><i class="fa fa-sort-desc"></i></td>
                            <?php }?>
                            <td><a href="#">Status</a><i class="fa fa-sort-desc"></i></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                
                                <?php if (($this->session->userdata('user'))=="") {?>
                                <a href="<?php echo base_url('admin/detail_order/');?>">ORD.001</a>
                                <?php }else{ ?>
                                ORD.001
                                <?php } ?>
                            </td>
                            <td>Roy</td>

                            <?php if (($this->session->userdata('user'))!="") {?>
                            <td>
                                <!--progress-->
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%;"></div>
                                </div>
                                <span class="progress-txt">status: 100%</span>
                                <!--/progress-->                         
                            </td>
                            <?php }?>
                            <td><span class="status status-done">Selesai</span></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="#">ORD.001</a>
                            </td>
                            <td>Roy</td>

                            <?php if (($this->session->userdata('user'))!="") {?>
                            <td>
                                <!--progress-->
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%;"></div>
                                </div>
                                <span class="progress-txt">status: 100%</span>
                                <!--/progress-->                         
                            </td>
                            <?php }?>
                            <td><span class="status status-pending">Menunggu</span></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="#">ORD.001</a>
                            </td>
                            <td>Roy</td>

                            <?php if (($this->session->userdata('user'))!="") {?>
                            <td>
                                <!--progress-->
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%;"></div>
                                </div>
                                <span class="progress-txt">status: 100%</span>
                                <!--/progress-->                         
                            </td>
                            <?php }?>
                            <td><span class="status status-failed">Gagal</span></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="#">ORD.001</a>
                            </td>
                            <td>Roy</td>

                            <?php if (($this->session->userdata('user'))!="") {?>
                            <td>
                                <!--progress-->
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%;"></div>
                                </div>
                                <span class="progress-txt">status: 100%</span>
                                <!--/progress-->                         
                            </td>
                            <?php }?>
                            <td><span class="status status-ongoing">Berlangsung</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->