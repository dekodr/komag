<style type="text/css">
    a{
        text-decoration: none;
    }
</style>
<!--.sideArea-->
<div class="sideArea">
    <!--.sideInner-->
    <div class="sideInner clearfix">
        <ul>
            <li class="<?php if(uri_string()=="admin/pedoman" || uri_string()=="admin/add_pedoman"|| uri_string()=="admin/add_struktur  "){echo "active";} ?>">
                <div>
                    <a href="<?php echo base_url('admin/pedoman');?>"><i class="fa fa-newspaper-o"></i> Pedoman</a>
                </div>
            </li>
          <!--   <li class="<?php if(uri_string()=="admin/komag" || uri_string()=="admin/add_komag"|| uri_string()=="admin/add_struktur  "){echo "active";} ?>">
                <div>
                    <a href="<?php echo base_url('admin/komag');?>"><i class="fa fa-newspaper-o"></i> Komag &amp; Struktur</a>
                </div>
            </li> -->
            <li class="<?php if(uri_string()=="admin/tabel" || uri_string()=="admin/tabel"|| uri_string()=="admin/tabel  "){echo "active";} ?>">
                <div>
                    <a href="<?php echo base_url('admin/tabel');?>"><i class="fa fa-table" aria-hidden="true"></i>Daftar Tabel</a>
                </div>
            </li>
            <li class="<?php if(uri_string()=="admin/material" || uri_string()=="admin/add_material"){echo "active";} ?>">
                <div>
                    <a href="<?php echo base_url('admin/material');?>"><i class="fa fa-object-group" aria-hidden="true"></i> KOMAG</a>
                </div>
            </li>
            <li style="background: #535c68!important">
                <div>
                    <a href="<?php echo base_url('admin/komag_table');?>"><i class="fa fa-download" aria-hidden="true"></i> Daftar Master KOMAG</a>
                </div>
            </li>
        </ul>
    </div>
    <!--/.sideInner-->    
</div>
<!--/.sideArea-->