<script>

    $(document).ready(function(){
        $( ".addColumn" ).on('click', function() {
            $( "#Form" ).append( '<option value="" selected="selected">Pilih salah satu</option><option value="1">KOMAG - 123</option></select>' );
        });

        $( ".komag" ).change(function() {
          // Check input( $( this ).val() ) for validity here
            var key = $(this).val();
            data = (key);
            // alert(data);

            $.ajax({
                url: "<?php echo base_url(); ?>/admin/get_id_table/",
                data: {data},
                dataType: "json",
                type: 'POST',
                // dataType: json,
                error: function(data){
                    console.log("GAGAL");
                },
                success: function(data) {
                    $('.short_desc').val(data.short[0]);
                    $('.long_desc').val(data.long[0]);
                    console.log(data);
                }             
            });

            
        });
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>


<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Tambah Master Komag</h1>
        <h3></h3>

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Form Tambah Master Komag</h3>
                
                <?php echo form_open_multipart('');?>
                    <div id="Form">
                    	
                        <input type="hidden" name="id_pedoman" value="<?php echo $id_pedoman;?>">
                        <div class="inputGroup clearfix">
                            <label for="title">Kelompok Material*</label>                            
                            <?php echo form_dropdown('id_material', $material, $this->form->get_temp_data('material'),'class="textInput"');?>
                        </div>
                        <div class="inputGroup clearfix komagkomag">
                            <label for="title">KOMAG*</label>
                            <input class="textInput komag" type="text" placeholder="Kode Material Gas" name="komag" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Short Description*</label>
                            <input class="textInput short_desc" type="text" placeholder="Short Description" name="short_desc" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Long Description*</label>
                            <input class="textInput long_desc" type="text" placeholder="Long Description" name="long_desc" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">UOM*</label>
                            <input class="textInput" type="text" placeholder="Unit of Measurement Code" name="uom" required>
                        </div>

                    </div>

                        <!-- <a href="#" class="addColumn"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a> -->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="submit" type="submit" class="buttonInput blueBG">
                            Simpan</i>
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea