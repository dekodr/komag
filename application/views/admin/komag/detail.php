<style>
    .dsc{    font-style: italic;
    padding: 5px;}
</style>
<script>
    $(document).ready(function(){
        $( ".addColumn" ).on('click', function() {
            $( "#Form" ).append( '<option value="" selected="selected">Pilih salah satu</option><option value="1">KOMAG - 123</option></select>' );
        });


        $( "#id_pedoman" ).change(function() {
            var id_pedoman = $(this).val();
            // alert(id_pedoman);
        });
        
        $( ".komag" ).change(function() {
          // Check input( $( this ).val() ) for validity here
          event.stopPropagation(); 
            var key = $(this).val();
            data = (key);
            // alert(data);

            $.ajax({
                url: "<?php echo base_url(); ?>/admin/get_id_table/",
                data: {data},
                dataType: "json",
                type: 'POST',
                // dataType: json,
                error: function(data){
                    console.log("GAGAL");
                },
                success: function(data) {

                    
                        $('.short_desc_').html(data.short);
                        $('.long_desc_').html(data.long);
                    console.log(data);
                }             
            }); 
        });

        $( "#link-table li p" ).on('click', function() {
            // alert($(this).attr('id'));
            var id_table = $(this).attr('id');
            $("#data").empty();
            $("#data").append("<img src='<?php echo base_url('assets/common/images/loading.gif')?>' width='450px'/> ");

            var key = $(this).val();
            data = (key);
            // alert(data);

            $.ajax({
                url: "<?php echo base_url(); ?>/admin/get_per_table/"+id_table,
                data: {data},
                dataType: "json",
                type: 'POST',
                // dataType: json,
                error: function(data){
                    console.log("GAGAL");
                },
                success: function(data) {
                    $("#data").empty();
                    $('#data').html(data.table_header);

                    $('.short_desc').html(data.short);
                    $('.long_desc').html(data.long);
                    $('.short_desc').html(data.short);
                    
                        $('.short_desc_').html(data.short);
                        $('.long_desc_').html(data.long);
                    console.log(data);
                }             
            }); 
        });
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
    input{
        width: calc(100% - 20px) !important;
    }

    label{
        width: 100% !important;
        text-align: left !important;
    }
</style>

<!--.contentArea-->
<div class="contentArea" style="overflow: hidden;">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Pedoman <?php echo $pedoman['id'].". ".$pedoman['name'];?></h1>s
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->

        <!--
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_pedoman') ?>" class="buttonA blueBG">Tambah</a></li>
            </ul>
        </div>
        lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <?php if($pedoman['pic'] !== null){?>
                <img src="<?php echo base_url('assets/pedoman/'.$pedoman['pic']);?>">
                <?php }else{?>
                <?php foreach ($pedoman_struktur as $key => $value) {
                    for ($i=0; $i < $value['digit']; $i++) { 
                        # code...
                        echo "X";
                    }

                    echo "     ";
                }}?>
                <br>
                <br>
                <br>
                <br>
                <h3>Keterangan: </h3>

                 <ol id="link-table" style="list-style-type: none; margin: 0;">   
                    <?php $no = 1; foreach ($komag as $key => $value) {?>
                    <li style="margin: 10px">
                        <?php 
                            if (substr($value['pedoman_struktur'], 0, 3) === ' - ' || substr($value['pedoman_struktur'], 0, 2) === '- ') {
                                echo "        ".$value['pedoman_struktur'];
                            }else{
                                echo $no.". ".$value['pedoman_struktur'];
                                $no++;
                            }
                            if (($value['table'])) {
                                # code...
                                echo "(";
                                foreach ($value['table'] as $key => $table_) {
                                    # code...
                                    echo "<p id='".$table_['id']."' href= '".base_url('admin/detail_tabel/'.$table_['id'])."' target='_blank' style='color: orange; text-decoration: underline;display: inline;'>".$table_['name']."<a href= '".base_url('admin/detail_tabel/'.$table_['id'])."' target='_blank'><i style='color: orange; text-decoration: underline;display: inline;' class='fa fa-external-link'></i></a></p>, ";
                                }
                                echo ").";
                            }
                        ;?>
                        
                    </li>
                    <?php }?>
                </ol>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

        <div class="lineArea clearfix" style="float: left; width: 48%; overflow-x: hidden; overflow: auto; height: 489px;">
            <div  id="data" class="tableInfo" style="height: 100%; padding-right: 17px; ">
                <p style="    font-size: 145px;text-align: center;font-weight: bold;color: gainsboro;line-height: 114px;margin: 50px 0;">YOUR TABLE HERE</p>
           </div>
        </div>

        <!--.lineArea-->
        <div class="lineArea clearfix" style="float: right; width: 48%">
            <!--.formArea-->
            <div class="tableInfo" id="tambahForm">
                <h2>Form Tambah Master Komag</h2>
                
                <?php echo form_open_multipart('');?>
                    <div id="Form">
                        <div class="inputGroup clearfix">
                            <label for="title">Pedoman > <?php echo $pedoman['name'];?></label>
                            <input class="textInput komag" type="hidden" name="id_material" value="<?php echo $pedoman['id'];?>" required>
                        </div>
                        <div class="inputGroup clearfix">
                            <label for="title">Kelompok Material*</label>                     
                            <?php echo form_dropdown('id_material', $material, $this->form->get_temp_data('material'),'class="textInput" id="id_material"');?>
                        </div>
                        <div class="inputGroup clearfix komagkomag">
                            <label for="title">KOMAG*</label>
                            <input class="textInput komag" type="text" placeholder="Kode Material Gas" name="komag" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Short Description*</label>
                            <textarea style="width='90%'; display:'block';" class="textInput short_desc" type="text" placeholder="Short Description" name="short_desc" required></textarea>
                            <p class="short_desc_ dsc"></p>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Long Description*</label>
                            <textarea style="width='90%'; display:'block';" class="textInput long_desc" type="text" placeholder="Long Description" name="long_desc" required></textarea>
                            <p class="long_desc_ dsc"></p>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">UOM*</label>
                            <?php echo form_dropdown('uom', $uom, $this->form->get_temp_data('uom'),'class="textInput" id="uom"');?>
                        </div>

                    </div>

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="submit" type="submit" class="buttonInput blueBG">
                            Simpan</i>
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->


    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->