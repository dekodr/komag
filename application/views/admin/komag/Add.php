<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script>

    $(document).ready(function(){
        $( ".addColumn" ).on('click', function() {
            $( "#Form" ).append( '<option value="" selected="selected">Pilih salah satu</option><option value="1">KOMAG - 123</option></select>' );
        });
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>


<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Tambah Pedoman Tabel</h1>
        <h3></h3>

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3> Pilih Tabel Untuk Tiap Pedoman</h3>
                
                <?php echo form_open_multipart('');?>
                    <div id="Form">
                       
                        <?php foreach ($struktur as $key => $value) {?>
                        <!--.inputGroup-->
                        <div class="inputGroup clearfix" style="width: calc(100% - 40px);border-bottom: 5px #eee solid; padding: 20px">
                            <label style="width: calc(98% - 40px);text-align: left;padding: 1%;"><?php echo $value['name'];?></label>
                            <input type="hidden" value="<?php echo $value['id'];?>" name="id_pedoman_struktur[]">
                            <?php echo form_dropdown('id_table[]', $table, $this->form->get_temp_data('table'),'class="textInput"  style="width: calc(98% - 40px);text-align: left;padding: 1%;"');?>
                        </div>
                        <!--/.inputGroup-->
                        <?php }?>

                       <!--  <div class="inputGroup clearfix">
                            <label for="title">KOMAG*</label>
                            <input class="textInput" type="text" placeholder="KOMAG" name="komag" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Short Description*</label>
                            <input class="textInput" type="text" placeholder="Nama" name="short_desc" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Long Description*</label>
                            <textarea class="textInput" type="text" placeholder="Nama" name="long_desc" required></textarea>
                        </div> -->
                    </div>

                        <!-- <a href="#" class="addColumn"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a> -->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <button style="margin: 10px; float: right;" name="submit" type="submit" class="buttonInput blueBG">
                            Simpan Komag <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea