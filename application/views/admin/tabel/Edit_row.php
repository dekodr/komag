<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="<?php echo base_url('assets/common/js/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/common/js/tinymce/tinymce.min.js'); ?>"></script>

<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
</script>
<script>
    tinymce.init({ 
        selector:'textarea',
        width: "80%",
        height: "150",
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>

<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Ubah <?php echo $row['table'];?></h1>
        <h3></h3>

        <!-- <?php print_r($news);?> -->
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Form Ubah <?php echo $row['table'];?></h3>
                
                <?php echo form_open_multipart('');?>
                    <?php foreach ($row['data'] as $key => $value) {?>
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="title"><?php echo $value['header'];?>*</label>
                        <input class="textInput" type="text" placeholder="<?php echo $value['header'];?>" name="<?php echo $value['id']?>" value="<?php echo $value['name'];?>">
                    </div>
                    <!--/.inputGroup-->
                    <?php }?>

                        <input class="textInput" type="hidden" name="id_table" value="<?php echo $value['id_table']?>" required>

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="edit" type="submit" class="buttonInput blueBG">
                            Simpan
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->