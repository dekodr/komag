<script>
    $(document).ready(function(){
        $(".inputGroup").hide();
        $( ".addColumn" ).on('click', function() {

            //TABLE
            $( ".table-body" ).append( "<tr>");
                <?php foreach ($table_header['column'] as $key => $column) {?>
                    $( ".table-body" ).append( "<td><input type='text' name='<?php echo $column['id']?>' placeholder='<?php echo $column['name']?>'></td>");
                <?php }?>
                $( ".table-body" ).append( "<td><a class='remove' href='#'><i class='fa fa-close'></i></a></td>");
            $( ".table-body" ).append( "</tr>");


            $(".inputGroup").show();
        });
        $(document).on('click', '.remove', function() {
            $(this).parents().eq(1).remove();
        });
    });
</script>

<style type="text/css">
    .table{
        border: 1px #d35400 solid !important;
    }
    .table thead tr{
        border: none !important;

    }
    .table thead tr td{
        background: #e67e22 !important;
        color: white !important;
        border: 1px #fff solid !important;
        font-weight: bold !important;

    }
    .table thead tr td a{
        color: white !important;
    }
    .table tbody td{
        border: 1px #d35400 solid !important;
    }
    .table input{
        padding: 5px;
    }
</style>


<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>TABEL <?php echo $table['table'];?></h1>
        <h3></h3>
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Tabel <?php echo $table['table'];?></h2>
                <?php echo form_open_multipart('');?>
                    <?php print_r($table_header['table']);?>
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button style="float: right; margin: 15px;" name="submit" type="submit" class="buttonInput blueBG">
                            Simpan
                        </button>
                    </div>
                </form>
                        <br><a href="#" class="addColumn"><i class="fa fa-plus-square-o" aria-hidden="true"></i>&nbsp;Tambah Baris</a>

            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->