<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/base.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/font-awesome.min.css">
       
        <title>Admin Login</title>

        <style type="text/css">
            .mainArea{
                width: 100%;
                min-height: 100%;
            }
            .loginWrapper{
                position: fixed;
                left: 0;
                right: 0;
                /*padding: 100px 0;*/
                width: 360px;
                margin: 0 auto;
                margin-top: 100px;
                z-index: 2;
                position: relative;
            }
            .formIn{
                z-index: 5;
            }
            .formBG{
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                position: absolute;
                top: 0;
                left: 0;
                /*display: block;*/
                z-index: 1;
                background: rgba(255,255,255, 0.3);
                -webkit-filter: blur(2px);
                -moz-filter: blur(2px);
                -o-filter: blur(2px);
                -ms-filter: blur(2px);
                filter: blur(2px);
            }

            .logoLogin{
                background: #fff;
                z-index: 5;
                border-radius: 0 20px 0 0; 
            }
            .formLogin{
                width: 360px;
                border-top: 5px #3498db solid;
                z-index: 5;
            }
            .formLogin div{
                margin: 20px;
                z-index: 5;
            }
            .formLogin h1{
                font-size: 18px;
                padding: 5px;
                color: #fff;
                /*font-weight: bold;*/
            }
            .formLogin input{
                padding: 10px;
                width: 300px;
                margin: 10px 0;
                border: 1px #eee solid;
            }
            .formLogin button{
                width: 320px;
                height: 40px;
                line-height: 40px;
                color: #fff;
                background: #3498db;
                margin: 15px 0;
                border: none;
                font-size: 16px;
                cursor: pointer;
            }
            .formLogin a{
                width: 360px;
                height: 40px;
                line-height: 40px;
                color: #666;
                text-align: center;
                display: block;
                background: #F2F2F2;
                border-radius: 0 0 0 20px;
            }
            .background-image {
                width: 100%;
                height: 100%;
                position: fixed;
                left: 0;
                right: 0;
                z-index: 1;
                display: block;

                /*background-image: url('https://d13yacurqjgara.cloudfront.net/users/370014/screenshots/1248918/attachments/169905/StudioWater-Stationary.png');
                background-size: cover;
                -webkit-filter: blur(10px);
                -moz-filter: blur(10px);
                -o-filter: blur(10px);
                -ms-filter: blur(10px);
                filter: blur(10px);*/
            }

            .error_msg{
                background: #fff;
                padding: 10px 15px;
                text-align: center;
                color: #c0392b;
            }
        </style>
    </head>
    
    <body>
    	<!--#wrap-->
        <div id="wrap">
            <!--#wrapInner-->
            <div id="wrapInner">
                <!--#mainArea-->
                <div class="mainArea clearfix">
                    <!-- <div class="background-image" id="gradient"></div> -->
                    <div class="loginWrapper">
                        <!--.logoLogin-->
                        <div class="logoLogin">
                            <img src="<?php echo base_url(); ?>assets/common/images/logo.pnasdg" alt="Komag" width="auto" height="80"/>
                        </div>
                        <!--/.logoLogin-->
                        <!--.formLogin-->
                        <form action="<?php echo site_url('main/login');?>" method="POST" class="formLogin shadow-soft">
                            <div>
                                <h1>Masuk ke akun Anda</h1>
                                <input type="text" placeholder="Username" name="username" required>
                                <input type="password" placeholder="Password" name="password" required>
                                <button type="submit" name="login">
                                    Masuk
                                </button>
                            </div>
                            <?php echo $this->session->flashdata('error_msg')?>
                            <a href="<?php echo base_url('main/forgot_password');?>">Lupa Password?</a>
                        </form>
                        <!--/.formLogin-->
                    </div>
                </div>
                <!--/#mainArea-->
            </div>
            <!--/#wrapInner-->
        </div>
        <!--/#wrap-->
    </body>


    <!--script-->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/jquery-latest.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/main.js"></script>
    <?php 
        if(isset($script)){
            echo $script;
        }
    ?>
    <!--/script-->
    <script type="text/javascript">
        var colors = new Array(
          [62,35,255],
          [60,255,60],
          [255,35,98],
          [45,175,230],
          [255,0,255],
          [255,128,0]);

        var step = 0;
        //color table indices for: 
        // current color left
        // next color left
        // current color right
        // next color right
        var colorIndices = [0,1,2,3];

        //transition speed
        var gradientSpeed = 0.002;

        function updateGradient()
        {
          
          if ( $===undefined ) return;
          
        var c0_0 = colors[colorIndices[0]];
        var c0_1 = colors[colorIndices[1]];
        var c1_0 = colors[colorIndices[2]];
        var c1_1 = colors[colorIndices[3]];

        var istep = 1 - step;
        var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
        var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
        var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
        var color1 = "rgb("+r1+","+g1+","+b1+")";

        var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
        var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
        var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
        var color2 = "rgb("+r2+","+g2+","+b2+")";

         $('#gradient').css({
           background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
            background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});
          
          step += gradientSpeed;
          if ( step >= 1 )
          {
            step %= 1;
            colorIndices[0] = colorIndices[1];
            colorIndices[2] = colorIndices[3];
            
            //pick two new target color indices
            //do not pick the same as the current one
            colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
            colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
            
          }
        }

        setInterval(updateGradient,10);
    </script>
</html>
