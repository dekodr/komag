<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Pencarian --- <?php echo $key;?></h1>

        
        <?php if(count($tabel) > 0){?>
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Daftar Tabel</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                            <td width=70px>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($tabel as $key => $value) {
                        ?>
                        <tr>
                            <td><a href="<?php echo base_url('admin/detail_tabel/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            <td>
                                &nbsp;                              
                                <a href="<?php echo base_url('admin/edit_tabel/');echo "/".$value['id'];?>"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url('lampiran/upload_tabel/');echo "/".$value['excel'];?>"><i class="fa fa-download"></i</a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->
        <?php }?>


        <?php if(count($pedoman) > 0){?>
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Pedoman</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">No</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                            <td width=70px>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($pedoman as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td><a href="<?php echo base_url('admin/detail_komag/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            <td><a href="<?php echo base_url('lampiran/upload_tabel/');echo "/".$value['excel'];?>"><i class="fa fa-download"></i</a></td>
                            <td>
                                &nbsp;
                                <a href="<?php echo base_url('admin/edit_pedoman/');echo "/".$value['id'];?>"><i class="fa fa-edit"></i></a>
                                <a onclick="return confirm('Hapus Item?')" href="<?php echo base_url('admin/delete_pedoman/');echo "/".$value['id'];?>"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->
        <?php }?>


        <?php if(count($komag) > 0){?>
         <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Komag</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Short Description</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Long Description</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Kelompok</a><i class="fa fa-sort-desc"></i></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($komag as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $value['komag'];?></td>
                            <td><?php echo $value['short_desc'];?></td>
                            <td><?php echo $value['long_desc'];?></td>
                            <td><?php echo $value['id_material'];?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->
        <?php }?>


        <?php if(count($material) > 0){?>
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Data Kelompok Material</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Code</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($material as $key => $value) {
                        ?>
                        <tr>
                            <td><a target="_blank" href="<?php echo base_url('admin/komag_table/');echo "/".$value['id'];?>"><?php echo $value['code'];?></a></td>
                            <td><a href="<?php echo base_url('admin/komag_table/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            
                        </tr>
                        <?php }?>

                        <tr>
                            <td colspan="2"><a target="_blank" href="<?php echo base_url('admin/komag_table/');?>">** MASTER KOMAG</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->
        <?php }?>

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->