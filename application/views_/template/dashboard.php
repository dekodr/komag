<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/dashboard.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/animate.css"> -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/reset.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/common/css/font-awesome.min.css">
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <title><?php if(isset($header)){echo $header;}?></title>
    </head>
    
    <body>
    	<!--#wrap-->
        <div id="wrap">
            <!--#wrapInner-->
            <div id="wrapInner">
                <!--#dashHeader-->
                <div id="dashHeader">
                    <ul class="clearfix">
                        <li class="logo">
                            <a href="<?php echo base_url(); ?>">
                                <img style="margin: 5px 0;" src="<?php echo base_url(); ?>assets/common/images/pgn.jpeg" alt="Perusahaan Gas Negara" width="auto" height="50"/>
                            </a>
                        </li>
                        <li class="search">
                            <form action="<?php echo base_url();?>" method="POST">
                                <input type="text" name="search" placeholder="Cari...">
                                <i class="fa fa-search"></i>
                            </form>
                        </li>
                        <li class="profil" style="background:url(<?php echo base_url();?>assets/common/images/profil.gif) no-repeat center; background-size: 50px;">
                            <div class="boxbg"></div>
                            <div class="box"></div>
                            <ul class="profilInside">
                                <li><a href="<?php echo base_url(); ?>main/profil"><i class="fa fa-cog"></i> Profil</a></li>
                                <li><a href="<?php echo base_url(); ?>main/logout"><i class="fa fa-sign-out"></i> Keluar</a></li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0)" onclick="maxWindow()">max</a>
                        </li> -->
                    </ul>
                </div>
                <!--/#dashHeader-->
                
                <!--#mainArea-->
                <div id="mainArea" class="clearfix">
                    <!--.sideArea-->
                    <?php if(isset($sideArea)){echo $sideArea;}?>
                    <!--/.sideArea-->

                    <!--.contentArea-->
                    <?php if(isset($contentArea)){echo $contentArea;}?>
                    <!--/.contentArea-->
                </div>
                <!--/#mainArea-->
            </div>
            <!--/#wrapInner-->
        </div>
        <!--/#wrap-->
    </body>


    <!--script-->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/jquery-latest.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/jquery-ui-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/popup.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/common/js/numeral.min.js"></script>

    <?php 
        if(isset($script)){
            echo $script;
        }
    ?>

    <script type="text/javascript">
        window.onload = maxWindow;

        function maxWindow() {
            window.moveTo(0, 0);


            if (document.all) {
                top.window.resizeTo(screen.availWidth, screen.availHeight);
            }

            else if (document.layers || document.getElementById) {
                if (top.window.outerHeight < screen.availHeight || top.window.outerWidth < screen.availWidth) {
                    top.window.outerHeight = screen.availHeight;
                    top.window.outerWidth = screen.availWidth;
                }
            }
        }
    </script>
    <!--/script-->
</html>
