<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="<?php echo base_url('assets/common/js/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/common/js/tinymce/tinymce.min.js'); ?>"></script>

<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
</script>
<script>
    tinymce.init({ 
        selector:'textarea',
        width: "80%",
        height: "150",
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>

<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Tambah Tabel</h1>
        <h3></h3>

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Form Tambah Tabel</h3>
                
                <?php echo form_open_multipart('');?>
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="title">Nama*</label>
                        <input class="textInput" type="text" placeholder="Nama" name="name" required>
                    </div>
                    <!--/.inputGroup-->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="submit" type="submit" class="buttonInput blueBG">
                            NEXT <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea