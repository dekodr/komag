<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="<?php echo base_url('assets/common/js/datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/common/js/tinymce/tinymce.min.js'); ?>"></script>

<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
    });
</script>
<script>
    tinymce.init({ 
        selector:'textarea',
        width: "80%",
        height: "150",
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>

<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Ubah News &amp; Event</h1>
        <h3></h3>

        <!-- <?php print_r($news);?> -->
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Form Ubah News &amp; Event</h3>
                
                <?php echo form_open_multipart('');?>
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="title">Judul*</label>
                        <input class="textInput" type="text" placeholder="Judul" name="title" value="<?php echo $news['title'];?>" required>
                    </div>
                    <!--/.inputGroup-->
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="date">Tanggal*</label>
                        <input class="textInput" id="datepicker" type="text" placeholder="Tanggal" name="date" value="<?php echo $news['date'];?>" required>
                    </div>
                    <!--/.inputGroup-->
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="content">Konten*</label>
                        <textarea class="textInput" placeholder="Isi Konten" name="content"><?php echo $news['content'];?></textarea>
                    </div>
                    <!--/.inputGroup-->
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="img1">Gambar 1*</label>
                        <div class="textInput">
                            <input class="" type="file" name="img1">
                            <p style="padding:3px; color: red;">*Tinggalkan kosong jika tidak ada perubahan</p>

                            <?php if (isset($news['img1'])) {?>
                            <img src="<?php echo base_url('lampiran/news_img/'); echo "/".$news['img1'];?>" width="50px">
                            <?php }?>
                        </div>
                    </div>
                    <!--/.inputGroup-->
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="img2">Gambar 2*</label>
                        <div class="textInput">
                            <input class="" type="file" name="img2">
                            <p style="padding:3px; color: red;">*Tinggalkan kosong jika tidak ada perubahan</p>

                            <?php if (isset($news['img2'])) {?>
                            <img src="<?php echo base_url('lampiran/news_img/'); echo "/".$news['img2'];?>" width="50px">
                            <?php }?>
                        </div>
                    </div>
                    <!--/.inputGroup-->
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label for="img3">Gambar 3*</label>
                        <div class="textInput">
                            <input class="" type="file" name="img3">
                            <p style="padding:3px; color: red;">*Tinggalkan kosong jika tidak ada perubahan</p>

                            <?php if (isset($news['img3'])) {?>
                            <img src="<?php echo base_url('lampiran/news_img/'); echo "/".$news['img3'];?>" width="50px">
                            <?php }?>
                        </div>
                    </div>
                    <!--/.inputGroup-->

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="publish" type="submit" class="buttonInput blueBG">
                            Simpan
                        </button>
                        <button name="preview" type="submit" class="buttonInput blueBG">
                            Preview
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->