<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Tabel</h1>
        <h3></h3>
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_tabel') ?>" class="buttonA blueBG">Tambah</a></li>
            </ul>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Daftar Tabel</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                            <td width=70px>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($table as $key => $value) {
                        ?>
                        <tr>
                            <td><a href="<?php echo base_url('admin/detail_tabel/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            <td>
                                &nbsp;
                                <a href="<?php echo base_url('admin/edit_table/');echo "/".$value['id'];?>"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url('admin/delete_table/');echo "/".$value['id'];?>"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>

                <!--.pagination-->
                <?php echo $pagination;?>
                <!--/.pagination-->
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->