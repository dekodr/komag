<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Komag</h1>
        <h3></h3>
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_data_komag') ?>" class="buttonA blueBG">Tambah</a></li>
            </ul>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Komag</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                            <td width=70px>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($pedoman as $key => $value) {
                        ?>
                        <tr>
                            <td><a href="<?php echo base_url('admin/detail_komag/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            <td>
                                &nbsp;
                                <a href="<?php echo base_url('admin/add_komag/');echo "/".$value['id'];?>"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>

                <!--.pagination-->
                <?php echo $pagination;?>
                <!--/.pagination-->
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->