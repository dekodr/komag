<script>

    $(document).ready(function(){
        $( ".addColumn" ).on('click', function() {
            $( "#Form" ).append( '<option value="" selected="selected">Pilih salah satu</option><option value="1">KOMAG - 123</option></select>' );
        });


        $( "#id_pedoman" ).change(function() {
            var id_pedoman = $(this).val();
            // alert(id_pedoman);
        });
        
        $( ".komag" ).change(function() {
          // Check input( $( this ).val() ) for validity here
            var key = $(this).val();
            data = (key);
            // alert(data);

            $.ajax({
                url: "<?php echo base_url(); ?>/admin/get_id_table/",
                data: {data},
                dataType: "json",
                type: 'POST',
                // dataType: json,
                error: function(data){
                    console.log("GAGAL");
                },
                success: function(data) {

                        $('.short_desc').val(data.short);
                        $('.long_desc').val(data.long);
                    

                        $('.short_desc').val(data.short);
                    console.log(data);
                }             
            }); 
        });

        $( "#link-table li" ).on('click', function() {
            // alert($(this).attr('id'));
            var id_table = $(this).attr('id');
            $("#data").empty();
            $("#data").append("<img src='<?php echo base_url('assets/common/images/loading.gif')?>' width='450px'/> ");

            var key = $(this).val();
            data = (key);
            // alert(data);

            $.ajax({
                url: "<?php echo base_url(); ?>/admin/get_per_table/"+id_table,
                data: {data},
                dataType: "json",
                type: 'POST',
                // dataType: json,
                error: function(data){
                    console.log("GAGAL");
                },
                success: function(data) {
                    $("#data").empty();
                    $('#data').html(data.table_header);

                    $('.short_desc').html(data.short);
                    $('.long_desc').html(data.long);
                    $('.short_desc').html(data.short);
                    console.log(data);
                }             
            }); 
        });
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
    input{
        width: calc(100% - 20px) !important;
    }

    label{
        width: 100% !important;
        text-align: left !important;
    }
</style>

<!--.contentArea-->
<div class="contentArea" style="overflow: hidden;">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Pedoman <?php echo $pedoman['name'];?></h1>s
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_pedoman') ?>" class="buttonA blueBG">Tambah</a></li>
            </ul>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <?php foreach ($pedoman_struktur as $key => $value) {
                    for ($i=0; $i < $value['digit']; $i++) { 
                        # code...
                        echo "X";
                    }

                    echo "     ";
                }?>
                <br>
                <br>
                <br>
                <br>
                <h3>Keterangan: </h3>

                <ol id="link-table" style="list-style-type: lower-alpha; margin: 0 0 0 30px;">   
                    <?php foreach ($komag as $key => $value) {?>
                    <li id="<?php echo $value['id_table'];?>" style="margin: 10px"><?php echo $value['pedoman_struktur']." ( <a href= '".base_url('admin/detail_tabel/'.$value['id_table'])."' target='_blank' style='color: orange; text-decoration: underline;'>".$value['table']."</a> ). ";?></li>
                    <?php }?>
                </ol>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

        <div class="lineArea clearfix" style="float: left; width: 48%; overflow: hidden; height: 489px;">
            <div  id="data" class="tableInfo" style="overflow-y: auto; height: 100%; padding-right: 17px; ">
                <p style="    font-size: 145px;text-align: center;font-weight: bold;color: gainsboro;line-height: 114px;margin: 50px 0;">YOUR TABLE HERE</p>
           </div>
        </div>

        <!--.lineArea-->
        <div class="lineArea clearfix" style="float: right; width: 48%">
            <!--.formArea-->
            <div class="tableInfo" id="tambahForm">
                <h2>Form Tambah Master Komag</h2>
                
                <?php echo form_open_multipart('');?>
                    <div id="Form">
                        <div class="inputGroup clearfix">
                            <label for="title">Pedoman > <?php echo $pedoman['name'];?></label>
                            <input class="textInput komag" type="hidden" name="id_material" value="<?php echo $pedoman['id'];?>" required>
                        </div>
                        <div class="inputGroup clearfix">
                            <label for="title">Kelompok Material*</label>                     
                            <?php echo form_dropdown('id_material', $material, $this->form->get_temp_data('material'),'class="textInput" id="id_material"');?>
                        </div>
                        <div class="inputGroup clearfix komagkomag">
                            <label for="title">KOMAG*</label>
                            <input class="textInput komag" type="text" placeholder="Kode Material Gas" name="komag" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Short Description*</label>
                            <input class="textInput short_desc" type="text" placeholder="Short Description" name="short_desc" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">Long Description*</label>
                            <input class="textInput long_desc" type="text" placeholder="Long Description" name="long_desc" required>
                        </div>

                        <div class="inputGroup clearfix">
                            <label for="title">UOM*</label>
                            <input class="textInput" type="text" placeholder="Unit of Measurement Code" name="uom">
                        </div>

                    </div>

                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="submit" type="submit" class="buttonInput blueBG">
                            Simpan</i>
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->


    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->