<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script>

    $(document).ready(function(){
        $( ".addColumn" ).on('click', function() {
            $( "#Form" ).append( '<div class="inputGroup clearfix"><label for="title">Nama*</label><input class="textInput" type="text" placeholder="Nama" name="name[]" required></div><div class="inputGroup clearfix"><label for="title">digit*</label><input class="textInput" type="text" placeholder="Nama" name="digit[]" required></div>' );
        });
    });
</script>
<style type="text/css">
    #tiny_mce,
    #mceu_13{
        float: left;
        padding: 6px 12px;
    }
</style>

<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Tambah Struktur Pedoman</h1>
        <h3></h3>

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.formArea-->
            <div class="formArea" id="tambahForm">
                <h3>Form Tambah Struktur Pedoman</h3>
                
                <?php echo form_open_multipart('');?>
                    <div id="Form">
                        <!--.inputGroup-->
                        <div class="inputGroup clearfix">
                            <label for="title">Nama*</label>
                            <input class="textInput" type="text" placeholder="Nama" name="name[]" required>
                        </div>
                        <div class="inputGroup clearfix">
                            <label for="title">digit*</label>
                            <input class="textInput" type="text" placeholder="digit" name="digit[]" required>
                        </div>
                        <!--/.inputGroup-->
                    </div>
                        <a  class="addColumn"><i class="fa fa-plus-square-o" aria-hidden="true"></i></a>
                    <!--.inputGroup-->
                    <div class="inputGroup clearfix">
                        <label></label>
                        <button name="submit" type="submit" class="buttonInput blueBG">
                            Simpan
                        </button>
                    </div>
                    <!--/.inputGroup-->
                </form>
            </div>
            <!--/.formArea-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->