<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Pedoman</h1>
        <h3></h3>
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_pedoman') ?>" class="buttonA blueBG">Tambah</a></li>
            </ul>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Pedoman <?php echo $pedoman['name'];?></h2>

                <?php foreach ($pedoman_struktur as $key => $value) {
                    for ($i=0; $i < $value['digit']; $i++) { 
                        # code...
                        echo "X";
                    }

                    echo "     ";
                }?>
                <br>
                <br>
                <br>
                <br>
                <h3>Keterangan: </h3>
                <ol style="list-style-type: lower-alpha; margin: 0 0 0 30px;">   
                    <?php foreach ($komag as $key => $value) {?>
                    <li  style="margin: 10px"><?php echo $value['pedoman_struktur']." ( <a href= '".base_url('admin/table_ex')."' target='_blank' style='color: orange; text-decoration: underline;'>".$value['table']."</a> ). ";?></li>
                    <?php }?>
                </ol>
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->