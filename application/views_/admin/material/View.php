<!--.contentArea-->
<div class="contentArea">
    <!--.contentInner-->
    <div class="contentInner clearfix">
        <h1>Material</h1>
        <h3></h3>
        
        <!--.lineArea-->
        <div class="lineArea">
            <?php echo $this->session->flashdata('msgSuccess')?>
            <?php echo $this->session->flashdata('msgError')?>
        </div>
        <!--/.lineArea-->
        <!--.lineArea-->
        <div class="lineArea clearfix">
            <ul class="menutab clearfix">
                <li><a href="<?php echo base_url('admin/add_data_komag') ?>" class="buttonA blueBG">Tambah KOMAG</a></li>
                <li><a style="margin-left: 10px;" href="<?php echo base_url('admin/export_komag') ?>" class="buttonA blueBG">Download KOMAG</a></li>
            </ul>
        </div>
        <!--/.lineArea-->

        <!--.lineArea-->
        <div class="lineArea clearfix">
            <!--.tableInfo-->
            <div class="tableInfo">
                <h2>Material</h2>
                <table>
                    <thead>
                        <tr>
                            <td><a href="#">Code</a><i class="fa fa-sort-desc"></i></td>
                            <td><a href="#">Nama</a><i class="fa fa-sort-desc"></i></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($material as $key => $value) {
                        ?>
                        <tr>
                            <td><a target="_blank" href="<?php echo base_url('admin/komag_table/');echo "/".$value['id'];?>"><?php echo $value['code'];?></a></td>
                            <td><a href="<?php echo base_url('admin/komag_table/');echo "/".$value['id'];?>"><?php echo $value['name'];?></a></td>
                            
                        </tr>
                        <?php }?>

                        <tr>
                            <td colspan="2"><a target="_blank" href="<?php echo base_url('admin/komag_table/');?>">** MASTER KOMAG</a></td>
                        </tr>
                    </tbody>
                </table>

                <!--.pagination-->
                <?php echo $pagination;?>
                <!--/.pagination-->
            </div>
            <!--/.tableInfo-->
        </div>
        <!--/.lineArea-->

    </div>
    <!--/.contentInner--> 
</div>
<!--/.contentArea-->