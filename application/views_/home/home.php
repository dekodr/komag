<!--#news-ticker-->
<div id="news-ticker-box" class="clearfix">
    <h2>BERITA TERBARU</h2>
    <div id="ticker">                        
        <div id="news-ticker">
            <ul>
                <?php foreach ($news as $key => $value) {?>
                <li>
                    <a href="<?php echo base_url('news?id='.$value['id']);?>"><?php echo $value['title'];?></a>
                </li>
                <?php }?>
            </ul>                    
        </div>
    </div>
</div>
<!--/#news-ticker-->

<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
		<td style="width: 700px;">					
			<ul class="bxslider news-preview">
		    	<?php $x = 0; foreach ($news as $key => $value) {?>
		      	<li <?php if(!$x) echo 'class="selected"'; ?> class="news-content top-content"><div><?php echo date("F j, Y, g:i a", strtotime($value['date']));?></div>
		      		<a href="<?php echo base_url('news?id='.$value['id']);?>">
						<?php if($value['img1']){ ?>
						<div class="coverImg">
							<img src="<?php echo base_url('lampiran/news_img')."/".$value['img1'];?>" alt="<?php echo $value['title'];?>">
						</div>
		      			<?php } else { ?>
		      			<div class="coverImg">
							<img src="<?php echo base_url('lampiran/news_img')."/default.jpg"; ?>" alt="<?php echo $value['title'];?>">
						</div>
		      			<?php } ?>
		      		</a>
		      	</li>
		      	<?php $x++; }?>
		    </ul>
		<!-- <div class="news-holder cf">
		    <ul class="news-headlines">
		    	<?php $x = 0; foreach ($news as $key => $value) {?>
		      	<li <?php if(!$x) echo 'class="selected"'; ?>><div><?php echo date("F j, Y, g:i a", strtotime($value['date']));?></div>
		      		<a href="<?php echo base_url('news?id='.$value['id']);?>">
		      			<?php echo $value['title'];?>
		      		</a>
		      	</li>
		      	<?php $x++; }?>
		    </ul>

		    <div class="news-preview">

		    	<?php foreach ($news as $key => $value) {?>
				<div class="news-content top-content">
		      		<a href="<?php echo base_url('news?id='.$value['id']);?>">
						<?php if($value['img1']){ ?>
						<img src="<?php echo base_url('lampiran/news_img')."/".$value['img1'];?>" alt="<?php echo $value['title'];?>">
		      			<?php } else { ?>
						<img src="<?php echo base_url('lampiran/news_img')."/default.jpg"; ?>" alt="<?php echo $value['title'];?>">
		      			<?php } ?>
		      		</a>
				</div>
		      	<?php }?>

		    </div>
		  </div>  -->

		</td>
	</tr>
	<tr>
		<td height="30px"></td>
	</tr>
	<tr>

		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="60%"><img src="<?php echo base_url('');?>assets/common/images/home-section-1.jpg"></td>
				<td width="40%">
					
					<div style="float: left;">
						<div class="title-header">Visi PPLPD</div>
						<div class="title-subheader">
							Terwujudnya Lembaga Pendidikan dan 
							Pelatihan Olahraga Pelajar Terbaik 
							di Indonesia Tahun 2020
						</div>
					</div>

					<div style="margin-top : 50px; float: left;">
						<div class="title-header">Misi PPLPD</div>
						<div class="title-subheader">
							<ul class="bxslider">
							  <li>Kegiatan Peningkatan Pembinaan dan Prestasi Olahraga Pelajar</li>
							  <li>Membangun dan Menanamkan Nilai-Nilai Universal Olahraga</li>
							  <li>Kedisiplinan</li>
							  <li>Pembinaan, Bimbingan &amp; Pelatiuhan Olahraga yang Terprogram dan Terukur</li>
							  <li>Mengaplikasikan Sport Sains, Sport Medicine</li>
							  <li>Mengembangkan Bakat dan Potensi Atlet Menuju Prestasi Optimal</li>
							  <li>Mewujudkan Ketersediaan Sarana &amp; Prasarana Olahraga Sesuai dengan Standar yang Dipersyaratkan</li>
							  <li>Mengembangkan dan Meningkatkan Profesionalisme Pelatih dan Pengelola PPLPD</li>
							</ul>
						</div>
					</div>

				</td>
			</tr>
		</table>

	</tr>
	<tr>
		<td height="100">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" valign="top">

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td width="32%">
						<div>
							<div class="title-header">Link Terkait</div>
							<div style="padding : 20px; font-size : 20px">
								<div style="padding : 5px"><a href="http://bogorkab.go.id" target="_blank">Website Resmi Kab. Bogor</a></div>
								<div style="padding : 5px"><a href="http://kemenpora.go.id" target="_blank">Website Resmi KEMENPORA</a></div>
							</div>
						</div>

						<div style="margin-top : 20px">
							<div class="title-header" style="margin-bottom : 5px">Anda Visitor ke </div>
							<div style="padding : 20px; padding-left : 40px; font-size : 20px">
								<img src="http://www.reliablecounter.com/count.php?page=pplpdbogorkab.com&digit=style/big/14/&reloads=1" alt="www.reliablecounter.com" title="www.reliablecounter.com" border="0"/>
							</div>
						</div>
					</td>
					<td width="20%" bgcolor="">
						
						<div style="max-width : 350px; max-height : 350px; overflow : hidden">
						<ul class="bxslider" style="width: 380px;">
						<li>
							<div class="calendar-tile">
								<div class="calendar-tile-date">MEI</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>ATLETIK</div>
										<div class="inner-small">PAPUA</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">MEI</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>TAEKWONDO</div>
										<div class="inner-small">SULAWESI SELATAN</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">MEI</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>ANGKAT BESI</div>
										<div class="inner-small">RIAU</div>
									</div>
								</div>	
							</div>		
							</li>

							<li>
							<div class="calendar-tile">
								<div class="calendar-tile-date">MEI</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>VOLI INDOOR</div>
										<div class="inner-small">KAB. BOGOR</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">JULI</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>DAYUNG</div>
										<div class="inner-small">SULAWESI TENGAH</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">AGTS</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>GULAT</div>
										<div class="inner-small">BANTEN</div>
									</div>
								</div>	
							</div>
							</li>

							<li>
							<div class="calendar-tile">
								<div class="calendar-tile-date">AGTS</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>PENCAK SILAT</div>
										<div class="inner-small">NTB</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">AGTS</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>SEPAK TAKRAW</div>
										<div class="inner-small">SULAWESI SELATAN</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">AGTS</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>VOLI PASIR</div>
										<div class="inner-small">SULAWESI SELATAN</div>
									</div>
								</div>	
							</div>
							</li>

							<li>
							<div class="calendar-tile">
								<div class="calendar-tile-date">SEPT</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>PANAHAN</div>
										<div class="inner-small">JAWA TIMUR</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">SEPT</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>TINJU</div>
										<div class="inner-small">MALUKU</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">NOV</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>TAEKWONDO</div>
										<div class="inner-small">SULAWESI UTARA</div>
									</div>
								</div>	
							</div>
							</li>

							<li>
							<div class="calendar-tile">
								<div class="calendar-tile-date">NOV</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>JUDO</div>
										<div class="inner-small">DKI JAKARTA</div>
									</div>
								</div>	
							</div>
							<div class="calendar-tile">
								<div class="calendar-tile-date">NOV</div>
								<div class="calendar-tile-detail">
									<div class="inner">
										<div>ANGGAR</div>
										<div class="inner-small">SUMATERA SELATAN</div>
									</div>
								</div>	
							</div>
							</li>
							</ul>
							<div class="bx-pager bx-default-pager">
								<div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="2" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="3" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="4" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="5" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="6" class="bx-pager-link">&nbsp;</a></div>
								<div class="bx-pager-item"><a href="" data-slide-index="7" class="bx-pager-link">&nbsp;</a></div>
							</div>

							<!--

							<ul class="bxslider">
								<li>

								<li>
									<div class="calendar-tile">
										<div class="calendar-tile-date">MEI</div>
										<div class="calendar-tile-detail">
											<div class="inner">
												<div>ATLETIK</div>
												<div class="inner-small">Jakarta</div>
											</div>
										</div>	
									</div>
</LI>
									<li>
									<div class="calendar-tile">
										<div class="calendar-tile-date">MEI</div>
										<div class="calendar-tile-detail">
											<div class="inner">
												<div>ANGKAT BESI</div>
											</div>
										</div>	
									</div>

									<li></LI>
									<div class="calendar-tile">
										<div class="calendar-tile-date">MEI</div>
										<div class="calendar-tile-detail">
											<div class="inner">
												<div>KARATE</div>
											</div>
										</div>	
									</div>
									<div style="clear : both"></div>
								</li></LI>

								<li>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">MEI</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>BOLA VOLI INDOR</div>
											</div>
										</div>
									</div>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">JULI</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>DAYUNG</div>
											</div>
										</div>	
									</div>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">AGT</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>GULAT </div>
											</div>
										</div>	
									</div>
									<div style="clear : both"></div>
								</li>
								<li>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">AGT</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>PENCAK SILAT</div>
											</div>
										</div>	
									</div>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">AGT</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>SEPAK TAKRAW</div>
											</div>
										</div>	
									</div>
									<div style="float: left;" class="calendar-tile">
										<div class="calendar-tile-date">AGT</div>
										<div class="calendar-tile-detail">
											<div id="inner">
												<div>BOLA VOLI PASIR</div>
											</div>
										</div>	
									</div>
									<div style="clear : both"></div>
								</li>

							</ul>							
								-->

						</div>
						<div style="clear : both"></div>

					</td>
					<td width="32%" align="center" valign="top">
						
						<div style="clear : both"></div>
						<div>
							<div style="float: left;" class="title-header">Kontak PPLPD</div>
							<div style="float: left; padding : 20px; font-size : 20px">
								
								<table cellspacing="0" cellpadding="0" border="0">
									<tr>
										<td valign="middle" width="60"><img src="<?php echo base_url('');?>assets/common/images/address.png" style="height : 40px"></td>
										<td valign="middle">
											Jl. Komp. pendidikan Kaumpandak No. 1 Karadenan<br>Cibinong - Bogor
										</td>
									</tr>
									<tr>
										<td valign="middle"><img src="<?php echo base_url('');?>assets/common/images/email.png" style="height : 40px"></td>
										<td valign="middle">pplpdkabbogor@gmail.com</td>
									</tr>
									<tr>
										<td valign="middle"><img src="<?php echo base_url('');?>assets/common/images/phone.png" style="height : 40px"></td>
										<td valign="middle">082221113206</td>
									</tr>
									<tr>
										<td colspan="2" height="50px"></td>
									</tr>
									<tr>
										<td colspan="2" align="center">
											<a href="https://www.instagram.com/pplpdbogorkab/" target="_blank"><span><img src="<?php echo base_url('');?>assets/common/images/instagram.png"/></span></a>
											<a href="https://www.youtube.com/channel/UC3X91kIqlXJoDfcBic-Zu9Q" target="_blank"><span><img src="<?php echo base_url('');?>assets/common/images/youtube.png"/></span></a>
										</td>
									</tr>
								</table>

							</div>		
						</div>
						
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
</table>