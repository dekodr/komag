<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Main_model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->library('session');

	}

	function cek_login(){

		$check = array(
						'username' => $this->input->post('username'), 
						'password' => $this->input->post('password')
					);
		/*check if username is exist*/
		$sql_un 	= "SELECT * FROM ms_login WHERE username = ? AND password = ? AND del = 0";
		$sql_un 	= $this->db->query($sql_un, $check);
		$sql_un 	= $sql_un->row_array();

		$ct_sql 	= '';

		if ($sql_un) {
			/*set the session*/
			$set_session = array(
				'id' 			=> 	$sql_un['id'],
				'username'		=>	$sql_un['username'],
				'password'		=>  $sql_un['password'],
			);
			$this->session->set_userdata('admin',$set_session);
			return "ok";
		}
	}

	function search($key){
		$data['material'] 	= $this->db->like('name', $key)->get('tb_material')->result_array();
		$data['komag'] 		= $this->db->like('komag', $key)->or_like('short_desc', $key)->or_like('long_desc', $key)->get('ms_komag')->result_array();
		$data['tabel']		= $this->db->like('name', $key)->where('del', null)->get('ms_table')->result_array();
		$data['pedoman'] 	= $this->db->like('name', $key)->get('ms_pedoman')->result_array();
		#print_r($data);
		return $data;
	}
	
}