<?php defined('BASEPATH') OR exit('No direct script access allowed');

class main extends CI_Controller {

	var $data;

	public function __construct(){

		parent::__construct();

		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel.php");
		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel/IOFactory.php");

		$this->load->library('form', 'database','url');
		$this->load->library('session');
		$this->load->model('Main_model', 'mm');
	}
	function zip(){
		$zip = new ZipArchive;
		if ($zip->open('lampiran/excel/TABEL - INSTR19.zip') === TRUE) {

			$tables = array();
			for( $i = 0; $i < $zip->numFiles; $i++ ){ 
			    $stat = $zip->statIndex( $i ); 
					array_push($tables, basename( $stat['name']));				
			}
			// print_r($tables);die;
			foreach ($tables as $name) {
				# code...

				$objPHPExcel = PHPExcel_IOFactory::load('./lampiran/excel/'.$name);
				$data = "";
				foreach($objPHPExcel->getWorksheetIterator() as $worksheet){
					$highestRow	= $worksheet->getHighestRow();
					// $table = $this->id_exploder($worksheet->getCell("A1")->getValue());

					$headerRow = 3;

					$lastColumn = $worksheet->getHighestColumn();
					$lastColumn++;
					/*for ($column = 'A'; $column != $lastColumn; $column++) {
					    $data['header'][] = $worksheet->getCell($column.$headerRow)->getValue();
					    //  Do what you want with the cell
					}*/	
						$data['title']  = $worksheet->getCell("A"."1")->getValue();
						/*$data['title']  = explode(".", $worksheet->getCell("A"."1")->getValue());
						$data['title']['code']  = $data['title'][0];
						$data['title']['name']  = $data['title'][1];*/

						$this->db->insert('ms_table', array('name' =>$data['title']));
						$id_table = $this->db->insert_id();

						for ($column = 'A'; $column != $lastColumn; $column++) {
						    $data['header'][$column] = $worksheet->getCell($column.$headerRow)->getValue();

							$this->db->insert('ms_table_header', array('name' => $data['header'][$column], 'id_table' => $id_table));
							$id_header = $this->db->insert_id();
						

						for($i=4;$i<=$highestRow;$i++){
							foreach ($data['header'] as $cell => $value) {
								# code...
								$data['main'][$i][$id_header] = $worksheet->getCell($cell.$i)->getValue();
							}
						}}
						$no = 1;
						foreach ($data['main'] as $data_) {
							# code...
							foreach ($data_ as $id_header_ => $value) {
								# code...
								$this->db->insert('ms_table_detail', array('id_table' => $id_table, 'id_header' => $id_header_, 'name' => $value, 'row' => $no));
							}
								$no++;

						}
				}
			}
			print_r($tables);
		    $zip->close();
		}else{
		    echo 'failed';
		}
	}
	function tes($to = "fadlimp@gmail.com'", $subject = "tes", $message = "TEST"){
		/*$config = Array(
				    'protocol' => 'smtp',
				    'smtp_host' => 'ssl://smtp.gmail.com',
				    'smtp_port' => 465,
				    'smtp_user' => 'fadlimp@gmail.com',
				    'smtp_pass' => 'Pratama\12345678',
				    // 'smtp_user' => 'pamsimaspusat@gmail.com',
				    // 'smtp_pass' => 'okpamsimas',
				    'mailtype'  => 'html', 
				    'charset'   => 'iso-8859-1',
				    'send_multipart' => FALSE
				);

		// $this->load->library('email');
		// $this->email->initialize($config);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('fadlimp@gmail.com', 'SMARTSIGHT');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);

		if($this->email->send()){
	    	echo 'Email sent.';
	    }else{
	    	show_error($this->email->print_debugger());
	    }*/
	    $config = Array(
				    'mailtype'  => 'text', 
				    'protocol' => 'smtp',
				    'smtp_host' => 'ssl://mail.pgnmas.co.id',
				    // 'smtp_host' => 'ssl://smtp.office365.com',
				    // 'smtp_port' =>  587,
				    'smtp_port' =>  25,
				    'smtp_user' => 'info@pgnmas.co.id',
				    'smtp_pass' => 'PGNMAS-33',
				    'charset'   => 'utf-8',
				    'smtp_timeout' => 30,
				    'crlf'		=> "\r\n",
				    'newline'	=> "\r\n",
				    'validate'	=> true,
				    // 'smtp_crypto' => 'tls',
				    'send_multipart' => FALSE,
				);

		// $this->load->library('email');
		// $this->email->initialize($config);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('info@pgnmas.co.id', 'TES');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()){
	    	echo 'Email sent.';
	    }else{
	    	show_error($this->email->print_debugger());
	    }
	
	}
	
	public function search(){
		$key = $_GET['q'];
		$data = $this->mm->search($key);
		$data['key'] = $key;

		$layout['header'] 		= 'Pencarian --- '.$key;
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('main/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function login(){

		if($this->input->post('username')&&$this->input->post('password')){
			// print_r($this->input->post());

			$is_logged = $this->mm->cek_login();

			if($is_logged == "ok"){
				/*if everything's cool*/			
				if($this->session->userdata('admin')){
					$data = $this->session->userdata('admin');
					redirect(base_url('admin'));
				}

			}else{
				/*if there's something wrong*/
				$this->session->set_flashdata('error_msg','<p class="error_msg">Data tidak dikenal. Silahkan login kembali!</p>');
				$this->load->view('main/login');			
			}
		}else{
			/*if there's empty textbox*/
			$this->session->set_flashdata('error_msg','<p class="error_msg">Isi form dengan benar!</p>');
			$this->load->view('main/login');
		}
	}

	public function redirect(){
		$this->load->view('main/redirect');
	}


	public function logout(){
		$sess = "";
		($this->session->userdata('user') != "" ) ? $sess = $this->session->userdata('user') : (($this->session->userdata('admin') != "" )? $sess = $this->session->userdata('admin'): $sess="") ;

		$this->session->sess_destroy();
		redirect(site_url());
	}
	
	
	public function index(){
		redirect(base_url('admin'));
	}

}