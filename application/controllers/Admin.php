<?php defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct(){

		parent::__construct();
		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel.php");
		require_once(APPPATH."third_party/phpexcel/Classes/PHPExcel/IOFactory.php");
		

		$this->load->model('admin_model','am');
		$this->load->library('form', 'database','url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('utility');
		$this->load->library('form_validation');
		$this->load->library('upload');
		  $this->load->library('session');
		//   $this->load->library('excel');


		if ($this->session->userdata('admin')){
        	return TRUE;
        }else{
        	redirect(base_url('main/login'));
        }

	}

	public function index(){
		$this->pedoman();
	}


	/*==============================================
						TABEL
	==============================================*/

	function read_table($id, $name){
		$objPHPExcel = PHPExcel_IOFactory::load('./lampiran/upload_tabel/'.$name);
				$data = "";
				foreach($objPHPExcel->getWorksheetIterator() as $worksheet){
					$highestRow	= $worksheet->getHighestRow();
					// $table = $this->id_exploder($worksheet->getCell("A1")->getValue());

					$headerRow = 3;

					$lastColumn = $worksheet->getHighestColumn();
					$lastColumn++;
					/*for ($column = 'A'; $column != $lastColumn; $column++) {
					    $data['header'][] = $worksheet->getCell($column.$headerRow)->getValue();
					    //  Do what you want with the cell
					}*/	
						$data['title']  = $worksheet->getCell("A"."1")->getValue();

						$id_table = $id;

						for ($column = 'A'; $column != $lastColumn; $column++) {
						    $data['header'][$column] = $worksheet->getCell($column.$headerRow)->getValue();

							$this->db->insert('ms_table_header', array('name' => $data['header'][$column], 'id_table' => $id_table));
							$id_header = $this->db->insert_id();
						

						for($i=4;$i<=$highestRow;$i++){
							foreach ($data['header'] as $cell => $value) {
								# code...
								$data['main'][$i][$id_header] = $worksheet->getCell($cell.$i)->getValue();
							}
						}}
						$no = 1;
						foreach ($data['main'] as $data_) {
							# code...
							foreach ($data_ as $id_header_ => $value) {
								# code...
								$this->db->insert('ms_table_detail', array('id_table' => $id_table, 'id_header' => $id_header_, 'name' => $value, 'row' => $no));
							}
								$no++;

						}
						// print_r($data);
				}
	}
	public function add_tabel(){

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			// print_r($_FILES);
			if(!empty($_FILES['excel']['name'])){
				$file 				= $this->upload_file($_FILES, 'excel');
			}

			// $_POST['excel']			= $file;

			
			$_POST['entry_stamp']	= date("Y-m-d H:i:s");

			$save_db = $this->db->insert('ms_table', $this->input->post());	
			$id_table = $this->db->insert_id();

			$upload_excel = $this->read_table($id_table, $file);
			// print_r($this->input->post());die;

			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
			redirect(site_url('/admin/detail_tabel/'.$id_table));
        }

		$layout['header'] 			= 'Tambah Master Tabel';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',null,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/tabel/Add',null,TRUE);
		$this->load->view('template/dashboard',$layout);
	}
	public function tabel(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['table'] 			= $this->am->get_table($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/tabel',$sort, $per_page, $this->am->get_table($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		

		$layout['header'] 		= 'Daftar Tabel';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function get_per_table($id_table){

		$data['table'] 			= $this->am->get_table_detail($id_table);
		$data['table_header'] 	= $this->am->get_table_header($id_table)['table'];

		print_r(json_encode($data));
	}
	public function detail_tabel($id_table){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['table'] 			= $this->am->get_table_detail($id_table);
		$data['table_header'] 	= $this->am->get_table_header($id_table);
		$column = array();
		$column					= $data['table_header']['column'];
		$row					= $data['table_header']['row'];

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$data = "";

			// print_r($this->input->post());die;
			// foreach ($this->input->post() as $column_ => $data) {
			// 	foreach ($data as $key => $value) {
			// 		$data_[$key][$column_] 	 	= $value;
			// 		$data_[$key]['id_table'] 	= $id_table;
			// 	}
			// }
			
			
				$row_ 					= $this->db->where('id_table', $id_table)->order_by('row', "DESC")->get('ms_table_detail')->row_array();
				$row_ 					= $row_['row'] +1;
				$row_count = count($row);

			foreach ($this->input->post() as $id_header_ => $post) {
				# code...
				$detail['save'][$id_header_]['id_header']	= $id_header_;
				$detail['save'][$id_header_]['name']		= $post;
				$detail['save'][$id_header_]['id_table']	= $id_table;
				$detail['save'][$id_header_]['row']	= $row_;
			}
				// $row_++;
			// foreach ($data_ as $key => $post) {
			// 	$detail['id_table'] 	= $id_table;
			// 	foreach ($column as $key_ => $value_) {
			// 		$id_header 					= $this->db->where('id_table', $id_table)->where('name', $value_['name'])->get('ms_table_header')->row_array()['id'];
			// 		$detail['save'][$id_header]['name'] = $post[preg_replace('!\s+!', '_', $value_['name'])];
			// 		$detail['save'][$id_header]['row'] 		= $row_;
			// 		$detail['save'][$id_header]['id_header']= $id_header;
			// 		$detail['save'][$id_header]['id_table'] = $id_table;

			// 	}
			// 	$row_++;
			// }
				#print_r($detail);die;
				foreach ($detail['save'] as $key => $value) {
					$this->db->insert('ms_table_detail', $value);
					#print_r($this->db->last_query());
				}
				#die;
				// $this->db->insert_batch('ms_table_detail', $detail['save']);
			// die;
			
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
			redirect(base_url('admin/detail_tabel/'.$id_table));
		}

		$layout['header'] 		= 'Daftar Tabel';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/detail',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function edit_tabel($id){
		if (isset($_POST['edit'])) {

			unset($_POST['edit']);
			$vld = 	array(
			array(
				'field'=>'title',
				'label'=>'Judul',
				'rules'=>'required'
				),
			);
 			

				$_POST['edit_stamp']	= date("Y-m-d H:i:s");
				$save_db 				= $this->am->edit_tabel($this->input->post(), $id);
 
				if($save_db){
					$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('admin/tabel/'));
				}
			//redirect ke halaman awal
			#redirect(site_url('admin/tabel'));
        }

        $data['tabel']			= $this->am->get_per_tabel($id);
        // print_r($data);
		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/Edit',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function delete_tabel($id){
		if($this->am->delete_tabel($id)){
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menghapus data!</p>');
			redirect(site_url('admin/tabel'));
		}else{
			$this->session->set_flashdata('msgSuccess','<p class="msgError">Gagal menghapus data!</p>');
			redirect(site_url('admin/tabel'));
		}
	}


	/*==============================================
					pedoman & EVENT
	==============================================*/
	public function pedoman(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['pedoman'] 		= $this->am->get_pedoman($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/pedoman',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Pedoman';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_pedoman(){
		if (isset($_POST['submit'])) {

				unset($_POST['submit']);
				$_POST['entry_stamp']	= date("Y-m-d H:i:s");
				// $save_db 				= $this->am->save_pedoman($this->input->post());
				$this->session->set_userdata('pedoman', $this->input->post());

				// print_r($this->session->userdata());die;

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/add_struktur/'));
        }

		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',NULL,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Add',NULL,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_struktur(){
		if (isset($_POST['submit'])) {
				unset($_POST['submit']);

				$this->session->set_userdata('pedoman_struktur', $this->input->post());
				$save_db 				= $this->am->save_pedoman();

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/add_komag/'.$save_db));
        }

		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',NULL,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Add_struktur',NULL,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function edit_pedoman($id){
		if (isset($_POST['edit'])) {

			unset($_POST['edit']);
			$vld = 	array(
			array(
				'field'=>'title',
				'label'=>'Judul',
				'rules'=>'required'
				),
			);
 			

				$_POST['edit_stamp']	= date("Y-m-d H:i:s");
				$save_db 				= $this->am->edit_pedoman($this->input->post(), $id);

				// if($save_db){
					$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('admin/pedoman/'));
				// }
			//redirect ke halaman awal
			redirect(site_url('admin/pedoman'));
        }

        $data['pedoman']			= $this->am->get_per_pedoman_($id);
        print_r($data);
		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Edit',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function delete_pedoman($id){
		if($this->am->delete_pedoman($id)){
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menghapus data!</p>');
			redirect(site_url('admin/pedoman'));
		}else{
			$this->session->set_flashdata('msgSuccess','<p class="msgError">Gagal menghapus data!</p>');
			redirect(site_url('admin/pedoman'));
		}
	}


	public function komag(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 10;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['pedoman'] 		= $this->am->get_pedoman($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/komag',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Komag';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/komag/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_komag($id_pedoman){

		if (isset($_POST['submit'])) {
			// print_r($this->input->post());
				unset($_POST['submit']);

				foreach ($this->input->post()['id_pedoman_struktur'] as $key => $value) {
					# code...
					$input[$key]['id_pedoman_struktur'] = $value;

				}

				foreach ($this->input->post()['id_table'] as $key => $value) {
					# code...
					$input[$key]['id_table']		= $value;
					// $input[$key]['komag']		= $this->input->post()['komag'];
					// $input[$key]['short_desc']	= $this->input->post()['short_desc'];
					// $input[$key]['long_desc']	= $this->input->post()['long_desc'];
					$input[$key]['entry_stamp']	= date("Y-m-d H:i:s");

				}
				// print_r($input);die;
					$save_db = $this->db->insert_batch('ms_komag_detail', $input);

					
				// print_r($this->session->userdata());die;

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/detail_komag/'.$id_pedoman));
        }

        $data['table']				= $this->am->get_table_id();
        $data['pedoman_struktur']	= $this->am->get_pedoman_struktur($id_pedoman);
        $data['struktur']			= $this->am->get_per_pedoman_struktur($id_pedoman);
		$layout['header'] 			= '';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function detail_komag($id_pedoman){
		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$_POST['entry_stamp']	= date("Y-m-d H:i:s");
			// print_r($this->input->post());die;

			$check 		= $this->db->where('komag', $this->input->post('komag'))->get('ms_komag')->row_array();
			if (count($check) > 0) {
				// print_r($check);die;
				$save_db 	= $this->db->where('id', $check['id'])->update('ms_komag', $this->input->post());
				$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Berhasil Diperbarui ! <a href="'.site_url('/admin/komag_table/'.$this->input->post()['id_material']).'">klik disini untuk melihat</a></p>');
				redirect(site_url('/admin/detail_komag/'.$id_pedoman));
				# code...
			}else{
				$save_db 	= $this->db->insert('ms_komag', $this->input->post());					
				$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan ! <a href="'.site_url('/admin/komag_table/'.$this->input->post()['id_material']).'">klik disini untuk melihat</a></p>');
				redirect(site_url('/admin/detail_komag/'.$id_pedoman));
			}
        }
        
        $data['id_pedoman']			= $id_pedoman;
        $data['komag']				= $this->am->get_komag($id_pedoman);
        $data['uom']				= $this->am->get_uom_opt();
        $data['pedoman_struktur']	= $this->am->get_per_pedoman($id_pedoman);
        $data['pedoman']			= $this->am->get_pedoman_($id_pedoman);
        $data['material']			= $this->am->get_material_id();        
        
		$layout['header'] 			= 'Detail Pedoman';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/detail',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function edit_komag($id_pedoman,$id_komag){
		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$_POST['edit_stamp']	= date("Y-m-d H:i:s");

			$save_db = $this->db->where('id', $id_komag)->update('ms_komag', $this->input->post());					
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
        }
        
        $data['id_komag']			= $id_komag;
        $data['id_pedoman']			= $id_pedoman;
        $data['komag']				= $this->am->get_komag($id_pedoman);
        $data['data']				= $this->am->get_per_komag($id_komag);
        $data['pedoman_struktur']	= $this->am->get_per_pedoman($id_pedoman);
        $data['pedoman']			= $this->am->get_pedoman_($id_pedoman);
        $data['material']			= $this->am->get_material_id();        
        
		$layout['header'] 			= 'Detail Komag';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Edit_',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function table_ex(){
		$this->load->view('admin/pedoman/MG01', null);
	}



	public function get_id_table(){
		$key = ($this->input->post());

		$data = explode(".", $key['data']);

		$return = $this->db->select('short_desc, long_desc')->where('komag', $key['data'])->get('ms_komag')->row_array();
		// print_r($return);die;
		if ($return != null) {
			$return['short'] = $return['short_desc'];
			$return['long'] = $return['long_desc'];
			unset($return['short_desc']);unset($return['long_desc']);
		}else{
			# code...

			$return['short'] 	= array();
			$return['long'] 	= array();
			$long  = $short  	= "";

			$i = 1;
			$total = count($data);
			foreach ($data as $q) {
				$q_ 	= $this->db->where('name', $q)->get('ms_table_detail')->row_array();
				$id  	= $q_['id'] + 1;

				if ($i == 1) {
					$sql = $this->db->select('*')->where('id_table', 1)->where('id', $id)->get('ms_table_detail')->row_array();
				}if ($i == 2) {
					$sql = $this->db->select('*')->where('id_table', 2)->where('id', $id)->get('ms_table_detail')->row_array();
				}if ($i == 3) {
					$q_ 	= $this->db->where('id_table', 3)->where('name', $q)->get('ms_table_detail')->row_array();
					$id  	= $q_['id'] + 1;
					$sql = $this->db->select('*')->where('id_table', 3)->where('id', $id)->get('ms_table_detail')->row_array();
				}if ($i == 4) {
					$sql = $this->db->select('*')->where('id_table', 4)->where('id', $q_['id'])->get('ms_table_detail')->row_array();
				}if ($i == 5) {
					$sql = $this->db->select('*')->where('id_table', 5)->where('id', $id)->get('ms_table_detail')->row_array();
				}
				// echo($id);
				if ($sql['name']) {
					# code...
					$short 	.=  ($sql['name']).", ";
				}
				if ($q_['id']) {
					# code...
					$long 	.=  $q_['name'].", ";
				}

				$i++;
				$return['short'] 	= array($short);
				$return['long']		= array($long);
			}
		}

		echo (json_encode($return, true));
		return  json_encode($return);
	}



	##################################################################
	####														  ####
	####     				KODE MATERIAL GAS 					  ####
	####														  ####
	##################################################################

	public function add_data_komag(){

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			// $_POST['entry_stamp']	= date("Y-m-d H:i:s");
			// print_r($this->input->post());die;

			// $save_db = $this->db->insert('ms_komag', $this->input->post());					
			// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
			redirect(site_url('/admin/detail_komag/'.$_POST['id_pedoman']));
        }

        $data['material']			= $this->am->get_material_id();       
        $data['pedoman']			= $this->am->get_select_id('ms_pedoman');
		$layout['header'] 			= 'Tambah Master Komag';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add_',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function komag_table($id_material = ""){

        $data['komag']				= $this->am->get_master_komag($id_material);
		$layout['header'] 			= 'Tabel Master Komag';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add_',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function edit_row($id_row){
		if (isset($_POST['edit'])) {

			unset($_POST['edit']);
			$vld = 	array(
			array(
				'field'=>'title',
				'label'=>'Judul',
				'rules'=>'required'
				),
			);
 			
			#print_r($this->input->post());die;
			foreach ($this->input->post() as $key => $value) {
				# code...
				$edit['name']		= $value;
				#$edit['edit_stamp']	= date("Y-m-d H:i:s");
				$save_db 			= $this->am->edit_row($edit, $key);
			}

				// if($save_db){
					$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('admin/detail_tabel/'.$this->input->post()['id_table']));
				// }
			//redirect ke halaman awal
			redirect(site_url('admin/detail_tabel').$this->input->post()['id_table']);
        }

        $data['row']			= $this->am->get_per_row($id_row);
        // print_r($data);
		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/Edit_row',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}
	

	public function delete_row($id_row){
		$row  = $this->db->where('id', $id_row)->get('ms_table_detail')->row_array();
		// $id_table = $this->am->delete_row($id_row);
		$id_table = $this->db->where('id_table', $row['id_table'])->where('row', $row['row'])->delete('ms_table_detail');
		if($id_table){
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menghapus data!</p>');
			redirect(site_url('admin/detail_tabel/'.$row['id_table']));
		}else{
			$this->session->set_flashdata('msgSuccess','<p class="msgError">Gagal menghapus data!</p>');
			redirect(site_url('admin/detail_tabel/'.$row['id_table']));
		}
	}
	public function add_material(){

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$_POST['entry_stamp']	= date("Y-m-d H:i:s");
			// print_r($this->input->post());die;

			$save_db = $this->db->insert('tb_material', $this->input->post());					
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
			redirect(site_url('/admin/material/'));
        }

		$layout['header'] 			= 'Tambah Master Material';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',null,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/material/Add',null,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function material(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 100;
		$sort 		= $this->utility->generateSort(array('code','name'));

		$data['material'] 		= $this->am->get_material($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/material',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Komag';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/material/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}




	public function upload_img($field, $db_name = ''){

		$file_name = $_FILES[$db_name]['name'] = $db_name.'_'.$this->utility->name_generator($_FILES[$db_name]['name']);
		
		$config['upload_path'] = './asset/images/';
		$config['allowed_types'] = 'jpeg|jpg|png|gif|';
		$config['max_size'] = '2096';
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($db_name)){
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('',''));
			return false;
		}else{
			$_POST[$db_name] = $file_name; 
			return true;
		}
	}
	public function upload_file($field, $db_name = ''){

		$file_name = $_FILES[$db_name]['name'] = $db_name.'_'.$this->utility->name_generator($_FILES[$db_name]['name']);
		
		$config['upload_path'] = './lampiran/upload_tabel/';
		$config['allowed_types'] = 'xlsx|xls';
		// $config['max_size'] = '209600';
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($db_name)){
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('',''));
			return false;
		}else{
			$_POST[$db_name] = $file_name; 
			return $file_name;
		}
	}

	public function export_komag(){

		$data = $this->am->get_all_komag();
		// print_r($data);die;
		$styleArray = array(
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),	
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),	
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),	
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'inside' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
			),
			'fill' => array(
	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'fff000')
	        ),
	        'font'  => array(
		        'bold'  => true,
		        'size'  => 12
		    )
		);

		$objPHPExcel = new PHPExcel();

			$objPHPExcel->createSheet(); 
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle(mb_strimwidth("DATA KOMAG", 0, 30, ""));
				
			$objPHPExcel->setActiveSheetIndex(0);

			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'NO');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'KOMAG');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Short Description');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Long Description');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Kelompok Material');

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			
			$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);

			foreach ($data as $key => $value) {
				# code...
				$nomor 	= $key + 1;
				$line 	= $nomor + 1;

					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$line, $nomor);
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$line, $value['komag']);
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$line, $value['short_desc']);
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$line, $value['long_desc']);
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$line, $value['id_material']);
			}

		$filename = 'Data KOMAG';

		$objPHPExcel->setActiveSheetIndex(0);  
               
              
		header("Content-Type: application/xls");    
		header("Content-Disposition: attachment; filename=$filename.xls");  
		header("Pragma: no-cache"); 
		header("Expires: 0");

		print_r($this->komag_table());
	}
	


}