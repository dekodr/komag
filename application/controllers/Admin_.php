<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_ extends CI_Controller {

	public function __construct(){

		parent::__construct();
		

		$this->load->model('admin_model','am');
		$this->load->library('form', 'database','url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('utility');
		$this->load->library('form_validation');
		$this->load->library('upload');
  		$this->load->library('session');


		if ($this->session->userdata('admin')){
        	return TRUE;
        }else{
        	redirect(base_url('main/login'));
        }

	}

	public function index(){
		$this->pedoman();
	}


	/*==============================================
						TABEL
	==============================================*/
	public function tabel(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['table'] 			= $this->am->get_table($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/table',$sort, $per_page, $this->am->get_table($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		

		$layout['header'] 		= 'Daftar Tabel';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function detail_tabel($id_table){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['table'] 			= $this->am->get_table_detail($id_table);
		$data['table_header'] 	= $this->am->get_table_header($id_table);
		$column = array();
		$column					= $data['table_header']['column'];

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$data = "";

			// print_r($this->input->post());
			foreach ($this->input->post() as $column_ => $data) {
				foreach ($data as $key => $value) {
					$data_[$key][$column_] 	 	= $value;
					$data_[$key]['id_table'] 	= $id_table;
				}
			}
			foreach ($data_ as $key => $post) {
				$detail['id_table'] 	= $id_table;
				foreach ($column as $key_ => $value_) {
					$detail[$value_['name']] 	= $post[$value_['name']];
					$detail['row'] 				= count($column)++;
					// print_r($post);
				}
				
				// $this->db->insert('ms_table_detail', $detail);
			}
  
			print_r($detail);die;
		}

		$layout['header'] 		= 'Daftar Tabel';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/tabel/detail',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	




	/*==============================================
					pedoman & EVENT
	==============================================*/
	public function pedoman(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 30;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['pedoman'] 		= $this->am->get_pedoman($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/pedoman',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Pedoman';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_pedoman(){
		if (isset($_POST['submit'])) {

				unset($_POST['submit']);
				$_POST['entry_stamp']	= date("Y-m-d H:i:s");
				// $save_db 				= $this->am->save_pedoman($this->input->post());
				$this->session->set_userdata('pedoman', $this->input->post());

				// print_r($this->session->userdata());die;

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/add_struktur/'));
        }

		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',NULL,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Add',NULL,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_struktur(){
		if (isset($_POST['submit'])) {
				unset($_POST['submit']);

				$this->session->set_userdata('pedoman_struktur', $this->input->post());
				$save_db 				= $this->am->save_pedoman();

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/add_komag/'.$save_db));
        }

		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',NULL,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Add_struktur',NULL,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function edit_pedoman($id){
		if (isset($_POST['preview'])) {

			unset($_POST['preview']);
			$vld = 	array(
			array(
				'field'=>'title',
				'label'=>'Judul',
				'rules'=>'required'
				),
			);
			if(!empty($_FILES['img1']['name'])){
			$vld[] = array(
					'field'=>'img1',
					'label'=>'File Excel',
					'rules'=>'callback_upload_img[img1]'
					);
			}
			if(!empty($_FILES['img2']['name'])){
			$vld[] = array(
					'field'=>'img2',
					'label'=>'File Excel',
					'rules'=>'callback_upload_img[img2]'
					);
			}
			if(!empty($_FILES['img3']['name'])){
			$vld[] = array(
					'field'=>'img3',
					'label'=>'File Excel',
					'rules'=>'callback_upload_img[img3]'
					);
			}
 			
			$this->form_validation->set_rules($vld);

			if ( $this->form_validation->run()==TRUE) {

				$upload_data = $this->upload->data();

			}


				$_POST['edit_stamp']	= date("Y-m-d H:i:s");
				$save_db 				= $this->am->edit_pedoman($this->input->post(), $id);

				// if($save_db){
					$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('admin/pedoman/'));
				// }
			//redirect ke halaman awal
			redirect(site_url('admin/pedoman'));
        }

        // $data['pedoman']			= $this->am->get_per_pedoman($id);

		$layout['header'] 		= '';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/pedoman/Edit',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function delete_pedoman($id){
		if($this->am->delete_pedoman($id)){
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Sukses menghapus data!</p>');
			redirect(site_url('admin/pedoman'));
		}else{
			$this->session->set_flashdata('msgSuccess','<p class="msgError">Gagal menghapus data!</p>');
			redirect(site_url('admin/pedoman'));
		}
	}


	public function komag(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 10;
		$sort 		= $this->utility->generateSort(array('date','title'));

		$data['pedoman'] 		= $this->am->get_pedoman($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/komag',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Komag';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/komag/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function add_komag($id_pedoman){

		if (isset($_POST['submit'])) {
			// print_r($this->input->post());die;
				unset($_POST['submit']);

				foreach ($this->input->post()['id_pedoman_struktur'] as $key => $value) {
					# code...
					$input[$key]['id_pedoman_struktur'] = $value;

				}

				foreach ($this->input->post()['id_table'] as $key => $value) {
					# code...
					$input[$key]['id_table']		= $value['id_table'];
					$input[$key]['komag']		= $this->input->post()['komag'];
					$input[$key]['short_desc']	= $this->input->post()['short_desc'];
					$input[$key]['long_desc']	= $this->input->post()['long_desc'];
					$input[$key]['entry_stamp']	= date("Y-m-d H:i:s");

				}
					$save_db = $this->db->insert_batch('ms_komag_detail', $input);
				// print_r($input);die;

					
				// print_r($this->session->userdata());die;

					// $this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
					redirect(site_url('/admin/detail_komag/'.$id_pedoman));
        }

        $data['table']				= $this->am->get_table_id();
        $data['pedoman_struktur']	= $this->am->get_pedoman_struktur($id_pedoman);
        $data['struktur']	= $this->am->get_per_pedoman_struktur($id_pedoman);
		$layout['header'] 			= '';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function detail_komag($id_pedoman){
        $data['komag']				= $this->am->get_komag($id_pedoman);
        $data['pedoman_struktur']	= $this->am->get_per_pedoman($id_pedoman);
        $data['pedoman']			= $this->am->get_pedoman_($id_pedoman);
        // print_r($data);
		$layout['header'] 			= 'Detail Pedoman';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/detail',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function table_ex(){
		$this->load->view('admin/pedoman/MG01', null);
	}



	public function get_id_table(){
		$key = ($this->input->post());

		$data = explode(".", $key['data']);

		$return['short'] 	= array();
		$return['long'] 	= array();
		$long  = $short  	= "";

		$total = count($data);
		foreach ($data as $q) {
			$sql = $this->db->where('code', $q)->get('ms_table_detail')->row_array();
			$short 	.=  $sql['name'].", ";
			$long 	.=  $sql['long_desc'].", ";
		}
			array_push($return['short'], $short);
			array_push($return['long'], $long);

		print_r(json_encode($return));
		return  json_encode($return);
	}



	##################################################################
	####														  ####
	####     				KODE MATERIAL GAS 					  ####
	####														  ####
	##################################################################

	public function add_data_komag(){

		if (isset($_POST['submit'])) {
			unset($_POST['submit']);
			$_POST['entry_stamp']	= date("Y-m-d H:i:s");
			// print_r($this->input->post());die;

			$save_db = $this->db->insert('ms_komag', $this->input->post());					
			$this->session->set_flashdata('msgSuccess','<p class="msgSuccess">Data Tersimpan!</p>');
			redirect(site_url('/admin/komag_table/'.$id_pedoman));
        }

        $data['material']			= $this->am->get_select_id('tb_material');
		$layout['header'] 			= 'Tambah Master Komag';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add_',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}


	public function komag_table($id_material = ""){

        $data['komag']				= $this->am->get_master_komag($id_material);
		$layout['header'] 			= 'Tabel Master Komag';
		$layout['sideArea'] 		= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 		= $this->load->view('admin/komag/Add_',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}

	public function material(){

		$search 	= $this->input->get('q');
		$page 		= '';
		$per_page	= 100;
		$sort 		= $this->utility->generateSort(array('code','name'));

		$data['material'] 		= $this->am->get_material($search, $sort, $page, $per_page,TRUE);
		$data['pagination'] 	= $this->utility->generate_page('admin/material',$sort, $per_page, $this->am->get_pedoman($search, $sort, '','',FALSE));
		$data['sort'] 			= $sort;
		// print_r($data);die;

		$layout['header'] 		= 'Komag';
		$layout['sideArea'] 	= $this->load->view('dashboard/SideArea',$data,TRUE);
		$layout['contentArea'] 	= $this->load->view('admin/material/View',$data,TRUE);
		$this->load->view('template/dashboard',$layout);
	}




	public function upload_img($field, $db_name = ''){

		$file_name = $_FILES[$db_name]['name'] = $db_name.'_'.$this->utility->name_generator($_FILES[$db_name]['name']);
		
		$config['upload_path'] = './asset/images/';
		$config['allowed_types'] = 'jpeg|jpg|png|gif|';
		$config['max_size'] = '2096';
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($db_name)){
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('',''));
			return false;
		}else{
			$_POST[$db_name] = $file_name; 
			return true;
		}
	}
	public function upload_file($field, $db_name = ''){

		$file_name = $_FILES[$db_name]['name'] = $db_name.'_'.$this->utility->name_generator($_FILES[$db_name]['name']);
		
		$config['upload_path'] = './lampiran/procurement_file/';
		$config['allowed_types'] = 'jpeg|jpg|png|gif|zip|pdf|rar';
		$config['max_size'] = '2096';
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if (!$this->upload->do_upload($db_name)){
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('',''));
			return false;
		}else{
			$_POST[$db_name] = $file_name; 
			return true;
		}
	}
    
}